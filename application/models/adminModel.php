<?php 

class adminModel extends CI_Model {



	public function totalEvents()
	{
	$sql = "SELECT count(eventId) as totalEvents from st_events where status = 'active'";
	$query = $this->db->query($sql)->result_array();;
	return $query[0]['totalEvents'];
	}
	public function totalUser()   //client
	{
	$sql = "SELECT count(userId) as totalUser from st_users where status = 'active' and userType = 'customer'";
	$query = $this->db->query($sql)->result_array();;
	return $query[0]['totalUser'];
	}
	public function totalRquests()
	{
	$sql = "SELECT count(carRequestId) as totalRquests from st_car_requests where (status = 'approved' or status = 'pending')";
	$query = $this->db->query($sql)->result_array();;
	return $query[0]['totalRquests'];
	}
	// public function totalWorkingLocation()
	// {
	// $sql = "SELECT count(locationId) as totalWorkingLocation from st_working_locations where status = 'active'";
	// $query = $this->db->query($sql)->result_array();;
	// return $query[0]['totalWorkingLocation'];		
	// }
	public function totalDrivers()
	{
	$sql = "SELECT count(st_users.userId) as totalDrivers from st_users JOin st_cars on st_cars.userId = st_users.userId where st_users.status = 'active' and userType = 'driver' and st_cars.status = 'active'";
	$query = $this->db->query($sql)->result_array();;
	return $query[0]['totalDrivers'];		
	}

//////////////////////////////
		public function checkUserNameAjax($userName)
{
  
$sql = "SELECT * FROM st_users WHERE userName='$userName'";
$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0) {
			return true;
		}
		else{
			return false;

		}
	
}
	
	//check current password is correct or not using ajax
	public function checkCurrentPasswordAjax($password)
{
  		$userId = $this->session->userdata('userIdAdmin');
		$this->db->Select('password');
		$this->db->from('st_users');
		$this->db->WHERE('userId',$userId);
		$this->db->WHERE('password',$password);
		$query = $this->db->get();
		
		
		if ($query->num_rows() == 1) {
			return true;
		}
		else{
			return false;

		}

}
	

     // post all the data of create user form
	public function post_createUser($data)
	{
		$this->db->Select('userName');
		$this->db->from('st_users');
		$this->db->WHERE('userName',$data['userName']);
		$query = $this->db->get();
		if ($query->num_rows() == 0 ) 
	{
	
		$this->db->Select('email');
		$this->db->from('st_users');
		$this->db->WHERE('email',$data['email']);
		$query = $this->db->get();

		if ($query->num_rows() == 0 ) 
		{

		$this->db->insert('st_users',$data);
		$this->session->set_flashdata('create_user_message', 'Record has been inserted successfully');
		redirect('adminController/createUser');
		}
		else 
		{
		$this->session->set_flashdata('no_email_message_user', 'Email must be unique');
		redirect('adminController/createUser');
		}
	}
	else 
		{
		$this->session->set_flashdata('no_userName_message_user', 'User Name must be unique');
		redirect('adminController/createUser');
		}
	}


	//get individual data of only user 
	public function individualViewUser($id)
	{
		$this->db->from('st_users');
		
		$this->db->where('userId',$id);
		$query = $this->db->get();
 
		return $query->row();

	}
	
	//get individual data of only user (update) 
	public function individualViewUserData($user_id)
	{
		$this->db->from('st_users');
		
		$this->db->where('userId',$user_id);
		$query = $this->db->get();
 
		return $query->result();

	} 

	//final update of user data
	public function updateViewUser_submit($userId,$data)
	{
		$this->db->WHERE('userId',$userId);
		$this->db->update('st_users',$data);
		$this->session->set_flashdata('updateViewUser_message', 'Update  successfully');
		 redirect('adminController/updateViewUser?id='.$userId);
	}

	 //get all user 
	public function getAllUser()
	{
		
		$sql = "SELECT su.userId, su.fullName, su.userName, su.email, su.phone, 
		(select count(st_assign_cars.assignId) 	from st_car_requests left join st_assign_cars on st_assign_cars.carRequestId = st_car_requests.carRequestId where st_car_requests.customerId=su.userId and st_assign_cars.status = 'active') 
		as totalCars from st_users su where su.status = 'active' and userType = 'customer' ";
		$query = $this->db->query($sql);
		//$this->db->from('st_users');
		//$query = $this->db->get();
		return $query->result();
	}
		public function deleteViewUser($userId)
	{
		
		$sql = "UPDATE st_users su set su.status = 'trash' where su.userId='$userId'";
		$sql1 =	"UPDATE st_car_requests sr set sr.status = 'trash' where sr.customerId='$userId'";
		

		$sql2 =	"UPDATE st_assign_cars sa set sa.status = 'trash' where  sa.carRequestId IN (select carRequestId from st_car_requests where customerId = '$userId')";


		$sql3 =	"UPDATE st_cars sc set sc.inUse = 0  where sc.carId IN (select carId from st_car_requests where carRequestId IN (select carRequestId from st_car_requests where customerId = '$userId'))";
		$sql4 = "UPDATE st_events se set se.status = 'trash' where se.userId='$userId'";
		
				
				$this->db->trans_start(TRUE);
				$this->db->query($sql);
				$this->db->query($sql1);
				$this->db->query($sql2);
				$this->db->query($sql3);
				$this->db->query($sql4);
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
					{
        				$this->db->trans_rollback();
        				$this->session->set_flashdata('client_Delete_error','An Error Occured ');
        				redirect('adminController/allUser');
					}
				else
					{
					    $this->db->trans_commit();
					    $this->session->set_flashdata('client_Delete_success','Deleted Successfully');
					    redirect('adminController/allUser');
					}
		
	}
	// public function deleteViewCar($carId)
	// {
	// 	$sql1 = "UPDATE st_users set status = 'trash'  where userId IN (select userId from st_cars where carId = '$carId')";
	// 	$sql2 = "UPDATE st_cars set status= 'trash'  where carId = '$carId'";
	// 	$this->db->query($sql1);
	// 	$this->db->query($sql2);
	// 	redirect('adminController/availableCar');
	// }


	// post all the working location areas from the form
	public function addWorkingLocation($data)
	{
		$this->db->insert('st_working_locations',$data);
		$this->session->set_flashdata('add_message', ' Area has been inserted successfully');
		redirect('adminController/addWorkingLocation');

	}


	// get all the working location areas 
	public function get_addWorkingLocation($status)
	{
		$this->db->select();
		$this->db->from('st_working_locations');
		$this->db->WHERE('status',$status);        //active
		$query = $this->db->get(); 
		return $query->result();
	}
	public function deleteWorkingLocation($status,$locationId)
	{
		$this->db->where('locationId',$locationId);
		$this->db->update('st_working_locations',$status);
		$this->session->set_flashdata('deleteWorkingLocation_message', 'Delete Working Location successfully');
		redirect('adminController/viewWorkingLocation');
	}


	//get a data of working location in update page for updation
	public function get_update_WorkingLocation($id)
	{
		$this->db->select();
		$this->db->from('st_working_locations');
		$this->db->WHERE('locationId',$id);
		$query = $this->db->get(); 
		return $query->result();
	}


	//submit button query for update the data in table
	public function updateWorkingLocation($locationId,$data)
	{
		$this->db->WHERE('locationId',$locationId);
		$this->db->update('st_working_locations',$data);
		$this->session->set_flashdata('updateWorkingLocation_message', 'Update Working Location successfully');
		redirect('adminController/viewWorkingLocation');


	}


	//post the data of new driver data in the table
	public function post_addCar($st_users_data,$st_cars_data)  //For New Driver
	{

		$this->db->Select('userName');
		$this->db->from('st_users');
		$this->db->WHERE('userName',$st_users_data['userName']);
		$query = $this->db->get();
	if ($query->num_rows() == 0 ) {
	
		$this->db->Select('email');
		$this->db->from('st_users');
		$this->db->WHERE('email',$st_users_data['email']);
		$query = $this->db->get();

	if ($query->num_rows() == 0 ) 
	{
			$this->db->insert('st_users',$st_users_data);
			$last_inserted_id = $this->db->insert_id();
		if ($last_inserted_id != null) {
			 
			$firstItem = array('userId' => $last_inserted_id);

			$st_cars_data = $firstItem + $st_cars_data;
			$this->db->insert('st_cars',$st_cars_data);
			$this->session->set_flashdata('message', 'Car has been inserted successfully');
		redirect('adminController/addCar');
			
		}
	}
	else 
	{
		$this->session->set_flashdata('no_email_message', 'Email must be unique');
		redirect('adminController/addCar');
	}
	}
	else 
	{
		$this->session->set_flashdata('no_userName_message', 'User Name must be unique');
		redirect('adminController/addCar');
	}
	
	}
	public function carModelId()
	{
		$this->db->from('st_car_models');
		$query = $this->db->get();
		return $query->result();
	}


	//post the existing car data which have assgned he driver into table
	public function post_existing_addCar($st_cars_data)
	{
		$this->db->insert('st_cars',$st_cars_data);
		$this->session->set_flashdata('exist_message', 'Car has been inserted successfully');
		redirect('adminController/addCar');		
	}


	//get those car which have not assigned the driver
	public function get_existing_car()
	{
		$sql = "SELECT u.userId,userName FROM st_users u Left JOIN st_cars c ON u.userId = c.userId WHERE u.userId NOT IN (SELECT userId FROM st_cars)";
		$query = $this->db->query($sql);
		return $query->result();
	}
	// public function allCar()
	// {
	// 	$this->db->from('st_cars');
	// 	$this->db->JOIN('st_users','st_users.userId = st_cars.userId');
	// 	$this->db->where('userType','driver');
	// 	$this->db->where('st_cars.status','active');
	// 	$this->db->where('st_users.status','active');
	// 	$query = $this->db->get();
 
	// 	return $query->result();

	// }

	//get those indiviudal car information  which have assigned the driver
	public function individual_viewCar($id)
	{

		$this->db->from('st_cars');
		$this->db->join('st_users','st_users.userId=st_cars.userId');
		$this->db->where('carId',$id);
		$query = $this->db->get();
 
		return $query->row();
	
	}
	//get those indiviudal car information  which have assigned the driver for update
	public function get_viewCar_update($carId)
	{

		$this->db->from('st_cars');
		$this->db->join('st_users','st_users.userId=st_cars.userId');
		$this->db->where('carId',$carId);
		$query = $this->db->get();
 
		return $query->result();
	
	}

	//final update the driver information 
	public function updateViewCar($userId,$carId,$st_users_data,$st_cars_data)
	{

		$this->db->update('st_users', $st_users_data, "userId = $userId");
		$this->db->update('st_cars', $st_cars_data, "carId = $carId");
		$this->session->set_flashdata('updateCar_message', 'Update successfully');
		redirect('adminController/updateViewCar?id='.$carId);

		
	}

	
	// admin login
	public function login($userName,$password,$userType)
	{
		$userType= 'admin';
		$sql = "SELECT * FROM `st_users` WHERE ((userName = '$userName') OR (email = '$userName')) AND (password = '$password') AND (userType = '$userType')";
		$query = $this->db->query($sql);
		
		if ($query->num_rows() == 1) {
			return $query->result_array();
		}
		else{
			return false;

		}
	}


	//change password of admin login
	public function change_password($cpassword,$userId)
	{
		$data = array
		(
               'password' => $cpassword,
              
        );
				$this->db->where('userId', $userId);
				$this->db->update('st_users', $data);
	}

//get the view event data from database(table)
 	public function viewEvent()
 	{
 		$sql = "SELECT *,st_events.status FROM `st_events` Join st_users on st_users.userId = st_events.userId where st_events.status = 'active' and st_users.status = 'active' and userType= 'customer'";
 		$query = $this->db->query($sql);
 		return $query->result();

 	}
 //get the individual view event data from database(table)
 	public function individualViewEvent($eventId)
 	{

 		$sql = "SELECT *,st_events.status FROM `st_events` Join st_users on st_users.userId = st_events.userId
 		 where eventId='$eventId'";
 		$query = $this->db->query($sql);
 		return $query->row();
 	
 	}
//get the individual view event data from database(table)
 	public function get_users()
 	{
 		$this->db->select('userId,fullName');
 		$this->db->from('st_users');
 		$this->db->where('userType','customer');    //client
 		$this->db->where('status','active');    //client

		$query = $this->db->get();
 		return $query->result();

 	}
 	public function createEvent($data){
 		$answer = $this->db->insert('st_events',$data);
 		if ($answer)
 		 {
			$this->session->set_flashdata('eventInsert_message', 'Inserted Successfully');
			redirect('adminController/createEvent');	
		}
	}

//get the individual view event data from database(IN update page)
 	public function getUpdateViewEvent($eventId)
 	{
 		$this->db->from('st_events');
 		$this->db->where('eventId',$eventId);
		$query = $this->db->get();
 		return $query->result();
 	}
 //final update the view event data 
 	public function updateViewEvent($data,$eventId)
 	{
 		$this->db->where('eventId', $eventId);
				$answer = $this->db->update('st_events', $data);

				if ($answer) {
					$this->session->set_flashdata('event_update_message', 'Update Successfully');
					redirect('adminController/updateViewEvent?id='.$eventId);	
					}
 	}

public function viewOnMap($eventId)
{
	$this->db->select();
	$this->db->from('st_events');
	$this->db->where('eventId',$eventId);
	$query = $this->db->get();
	return $query->result();
}
// //approved event by  admin
// 	public function approveViewEvent($eventId)
// 	{
// 		$data = array(
// 			'status' =>"active",
// 			'adminStatus' => $this->session->userdata('fullNameAdmin'),
// 			 );
// 		$userIdAdmin = $this->session->userdata('userIdAdmin');
// 		$this->db->WHERE('eventId',$eventId);
// 		$this->db->update('st_events',$data);
// 		$this->session->set_flashdata('approveViewEvent_message', ' Approve successfully');
// 		redirect('adminController/viewEvent');
// 	} 
	public function deleteViewEvent($eventId)
	{
		$data = array(
			'status' =>"trash",
			'adminStatus' => $this->session->userdata('fullNameAdmin'),
			 );
		$userIdAdmin = $this->session->userdata('userIdAdmin');
		$this->db->WHERE('eventId',$eventId);
		$this->db->update('st_events',$data);
		$this->session->set_flashdata('deleteViewEvent_message', 'Event Deleted successfully');
		redirect('adminController/viewEvent');
	}

	public function getAllRequest()
	{
		
		// $sql = "SELECT sc.*,st_users.*,sc.status as scStatus, count(st_assign_cars.assignId) as noOfAssignedCars from st_car_requests sc left JOIN st_assign_cars on st_assign_cars.carRequestId = sc.carRequestId and st_assign_cars.status = 'active' JOIN st_users ON st_users.userId=sc.customerId and st_users.status='active' group by sc.carRequestId";  left join hataya hai ke delete ke bd sara data laraha tha
		$sql = "SELECT sc.*,st_users.*,sc.status as scStatus, count(st_assign_cars.assignId) as noOfAssignedCars from st_car_requests sc  left JOIN st_assign_cars on st_assign_cars.carRequestId = sc.carRequestId and st_assign_cars.status = 'active' JOIN st_users ON st_users.userId=sc.customerId and st_users.status='active' WHERE sc.status != 'trash' group by sc.carRequestId";
			$query = $this->db->query($sql)->result();
  		
 			return $query;

	}
	public function allRequestDelete($carRequestId)
	{
		$sql = "UPDATE st_car_requests set status = 'trash' where carRequestId='$carRequestId'";
		$sql1 = "UPDATE st_assign_cars set status = 'trash' where carRequestId='$carRequestId'";

		$sql2 = "UPDATE st_users set status = 'active' where userId IN (Select customerId from st_assign_cars  
		where carRequestId='$carRequestId')";
		$sql3 = "UPDATE st_cars set inUse = 0 where carId IN (SELECT carId from st_assign_cars 
		where carRequestId='$carRequestId')";


		$this->db->trans_start(TRUE);
				$this->db->query($sql);
				$this->db->query($sql1);
				$this->db->query($sql2);
				$this->db->query($sql3);
				
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
					{
        				$this->db->trans_rollback();
        				$this->session->set_flashdata('allRequest_Delete_error','An Error Occured ');
        				redirect('adminController/allRequest');
					}
				else
					{
					    $this->db->trans_commit();
					    $this->session->set_flashdata('allRequest_Delete_success','Deleted Successfully');
					    redirect('adminController/allRequest');
					}

		
	}

	//car request 
 	public function carRequest($data)
 	{
 		$this->db->insert('st_car_requests',$data);
 		$insert_id = $this->db->insert_id();
 		// $this->session->set_flashdata('carRequest_message', 'Car Request Successfully');
 		redirect('adminController/requestApprovals?id='.$insert_id);
 	}
 
 	public function requestApprovals($inUse = '')       //available Car Controller    //getAssignCars Controller
 		
 	{
 		$this->db->from('st_cars');
 		$this->db->Join('st_users','st_users.userId=st_cars.userId');
 		if($inUse != '')
 		{
 			$this->db->WHERE('inUse',$inUse);
 		}
 		
 		$this->db->WHERE('st_cars.status','active');
 		$this->db->WHERE('st_users.status','active');
 		$query = $this->db->get();

 		return $query->result();
 	}

 	public function deleteAllCar($id,$redirect)
 	{
 		
 		if ($redirect == 'viewCar') {
 		
 		//echo "in the view car"; exit;
 		$sql2 = "UPDATE st_assign_cars SET status = 'trash' WHERE carId = '$id' ";
 		$sql1 = "UPDATE st_cars SET inUse = 0 WHERE carId = '$id'";

		$this->db->query($sql2);
		$this->db->query($sql1);
		//echo $this->db->last_query(); exit;
	}
	else
	{
		//echo "in the delete all car"; exit;
		$sql = "UPDATE st_users set status = 'trash'  where userId IN (select userId from st_cars where carId = '$carId')";
 		$sql1 = "UPDATE st_cars SET inUse = 0,status= 'trash' WHERE carId = '$id'";
 		$sql2 = "UPDATE st_assign_cars SET status = 'trash' WHERE carId = '$id' ";
		$this->db->query($sql);
 		$this->db->query($sql1);
	}
 	redirect('adminController/'.$redirect);

 	}
 	


 	public function getNoOfCars($carRequestId)
 	{
 			$sql = "SELECT noOfCars FROM `st_car_requests` WHERE carRequestId= '$carRequestId' and (status = 'approved' OR status = 'pending')";

 			$newSql = "SELECT count(*) as noOfAssignedCars from st_assign_cars where carRequestId = '$carRequestId' and status= 'active'";

 			$newQuery = $this->db->query($newSql)->result_array();

 			$totalAssignCars = $newQuery[0]['noOfAssignedCars'];
			
			$query = $this->db->query($sql)->result_array();
 			


 			$remainingCars = $query[0]['noOfCars'] - $newQuery[0]['noOfAssignedCars'];
			$query[0]['noOfAssignedCars'] = $newQuery[0]['noOfAssignedCars'];
 			$query[0]['remainingCars']  = $remainingCars;
 			
 			

 		
		
 		return $query;
 	}
 	public function UpdateRequestApprovals($carRequestId)
 	{
 		$sql = "SELECT * from st_assign_cars Join st_users on st_users.userId=st_assign_cars.customerId
 		Join st_cars on st_cars.carId=st_assign_cars.carId  where carRequestId = '$carRequestId' and st_assign_cars.status ='active' ";
 		$query = $this->db->query($sql)->result();
 		return $query;
 	}

public function deleteUpdateApprovals($carId)
{

	$sql = "UPDATE st_cars set inUse = 0 where carId='$carId'";
	$sql1 = "UPDATE st_assign_cars set status = 'trash' where carId='$carId'";	

	$this->db->trans_start(TRUE);
				$this->db->query($sql);
				$this->db->query($sql1);

				
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
					{
        				$this->db->trans_rollback();
        				redirect('adminController/allUser');
        				
					}
				else
					{
					    $this->db->trans_commit();
					    redirect('adminController/allUser');
					  
					}


}

 	public function assignSingleCar($carId,$createDate,$carRequestId)
 	{
 		
 		$this->db->WHERE('carId',$carId);
		$query = $this->db->update('st_cars',array('inUse' => 1, ));

 		if ($query) 
 		{
 			
 		
 		$this->db->select('userId');
 		$this->db->from('st_cars');
 		$this->db->WHERE('carId',$carId);
 		$query = $this->db->get();
 		$answer = $query->result_array();
 		$userId = $answer[0]['userId'];

 		if ($query) 
 		{
 			$data = array(
 			'carRequestId' => $carRequestId,
 			'createDate' => $createDate,
 			'customerId' =>$userId,
 			'carId' =>$carId,
 			 );
 		 	$this->db->insert('st_assign_cars',$data);
 		 	$sql = "UPDATE st_car_requests SET status = 'approved' WHERE carRequestId = '$carRequestId'";
 		 	$this->db->query($sql);
 		 	redirect('adminController/requestApprovals?id='.$carRequestId);

 		 } 
 		}
 	}

 	public function assignSelectedCar($selectedCarId,$createDate,$carRequestId)
 	{
 		for ($i=0; $i <count($selectedCarId) ; $i++) { 
 			
 			$this->db->WHERE('carId',$selectedCarId[$i]);
			$query = $this->db->update('st_cars',array('inUse' => 1,));


 		
 		
 			if ($query) 
 		{
 			
 		
 		$this->db->select('userId');
 		$this->db->from('st_cars');
 		$this->db->WHERE('carId',$selectedCarId[$i]);
 		$query = $this->db->get();
 		$answer = $query->result_array();
 		$userId = $answer[0]['userId'];

 		if ($query) {
 			$data = array(
 			'carRequestId' => $carRequestId,
 			'createDate' => $createDate,
 			'customerId' =>$userId,
 			'carId' =>$selectedCarId[$i],
 			 );
 		 	$this->db->insert('st_assign_cars',$data);

 		 } 
 	}
 	}
 			$data = array(
			'status' =>"approved",
			'approveBy' => $this->session->userdata('fullNameAdmin'),
			 );
 			
			$this->db->WHERE('carRequestId',$carRequestId);
			$this->db->update('st_car_requests',$data);

			$this->session->set_flashdata('assignSelectedCar_message', ' Assign Cars successfully');
			redirect('adminController/allRequest');
 	
 	}

 	public function userCars($userId)
 	{
 		// $sql = "select ac.carModel, ac.registrationNumber,au.fullName,au.phone from st_assign_cars ast left join st_cars ac on ac.carId = ast.carId left join st_users au on au.userId = ac.userId where ast.customerId = $userId";
 		$sql = "SELECT * from st_car_requests left join st_assign_cars on st_assign_cars.carRequestId = st_car_requests.carRequestId left join st_cars on st_cars.carId=st_assign_cars.carId
 		left join st_users on st_cars.userId=st_users.userId
 		 where st_car_requests.customerId='$userId' and st_assign_cars.status='active' and st_car_requests.status = 'approved' and st_cars.status = 'active'  and st_users.status = 'active' ";
 		$query = $this->db->query($sql)->result();
 		//print_r($query);exit();
 		return $query;
 	}
 	

 	public function checkAvailabelCars($noOfCar)
 	{
 		$sql = "SELECT Count(inUse) as total FROM `st_cars` WHERE inUse = 0 and status = 'active'";
 		$query =  $this->db->query($sql)->result_array();
 		 return $query[0]['total'];
 	}


	
}
?>
