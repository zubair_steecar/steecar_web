<?php 

class customerModel extends CI_Model {


     

//index page count 
	public function counts($userId)
	{
		$sql = "SELECT count(eventId) as totalEvents FROM `st_events` WHERE userId = '$userId' and status = 'active'";
		$query = $this->db->query($sql)->result_array();
		return $query[0]['totalEvents'];
		if (isset($query1[0]['totalEvents']))
		 {
		 	return $query1[0]['totalCarsRequest'];
		}
		else
		{
			return '0';
		}
		
	
	}
	public function counts1($userId)
	{
		$sql1 = "SELECT count(sc.carRequestId) as totalCarsRequest 
 			from st_car_requests sc left JOIN st_assign_cars on st_assign_cars.carRequestId = sc.carRequestId and st_assign_cars.status = 'active' WHERE sc.customerId ='$userId' AND (sc.status = 'approved' OR sc.status='pending') group by sc.carRequestId";
		$query1 = $this->db->query($sql1)->result_array();
		if (isset($query1[0]['totalCarsRequest']))
		 {
		 	return $query1[0]['totalCarsRequest'];
		}
		else
		{
			return '0';
		}
		
		
	}
	public function deleteUpdateApprovals($carId)
{

	$sql = "UPDATE st_cars set inUse = 0 where carId='$carId'";
	$sql1 = "UPDATE st_assign_cars set status = 'trash' where carId='$carId'";	

	$this->db->trans_start(TRUE);
				$this->db->query($sql);
				$this->db->query($sql1);

				
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
					{
        				$this->db->trans_rollback();
        				redirect('customerController/allRequest');
        				
					}
				else
					{
					    $this->db->trans_commit();
					    redirect('customerController/allRequest');
					  
					}


}

	public function counts2($userId)
	{
			$sql2 = "SELECT count(assignId) as totalAssignedCar from st_car_requests left join st_assign_cars on st_assign_cars.carRequestId = st_car_requests.carRequestId left join st_cars on st_cars.carId=st_assign_cars.carId left join st_users on st_cars.userId=st_users.userId where st_car_requests.customerId='$userId' and st_assign_cars.status='active' and st_car_requests.status = 'approved' and st_cars.status = 'active' and st_users.status = 'active'";
			$query2 = $this->db->query($sql2)->result_array();
			return $query2[0]['totalAssignedCar'];
			if (isset($query2[0]['totalAssignedCar']))
		 {
		 	return $query1[0]['totalCarsRequest'];
		}
		else
		{
			return '0';
		}
	}
// customer login
	public function customerLogin($userName,$password,$userType)
	{
		
		$sql = "SELECT * FROM `st_users` WHERE ((userName = '$userName') OR (email = '$userName')) AND (password = '$password') AND (userType = '$userType') AND (status = 'active')";
		$query = $this->db->query($sql);
		
		if ($query->num_rows() == 1) {
			return $query->result_array();
		}
		else{
			return false;

		}
	}


	public function change_password($cpassword,$userId)
	{
	
		$data = array(
               'password' => $cpassword,
              
            );

				$this->db->where('userId', $userId);
				$answer = $this->db->update('st_users', $data);

				if ($answer) {
					$this->session->set_flashdata('password_message', 'Your Password has been changed');
					redirect('customerController/carRequest');	
					}
		
		
	
}

//post the view event data in the database from creatEvent page
 	public function createEvent($data)
 	{
 		$answer = $this->db->insert('st_events',$data);
 		if ($answer)
 		 {
			$this->session->set_flashdata('event_insert_message', 'Inserted Successfully');
			redirect('customerController/createEvent');	
		}

 	}

 	public function carDetails($userId)
 	{
 	// 	$this->db->from('st_cars');
 	// 	$this->db->WHERE('userId',$userId);
		// $query = $this->db->get();
 	// 	return $query->result();	
 		$sql = "SELECT * from st_car_requests left join st_assign_cars on st_assign_cars.carRequestId = st_car_requests.carRequestId left join st_cars on st_cars.carId=st_assign_cars.carId left join st_users on st_cars.userId=st_users.userId where st_car_requests.customerId='$userId' and st_assign_cars.status='active' and st_car_requests.status = 'approved' and st_cars.status = 'active' and st_users.status = 'active'";
 		return $this->db->query($sql)->result();
 	}
 	public function assignCarDetails($carRequestId)
 	{
 		$userId = $this->session->userdata('userId');
 		$sql = "SELECT * from st_car_requests left join st_assign_cars on st_assign_cars.carRequestId = st_car_requests.carRequestId left join st_cars on st_cars.carId=st_assign_cars.carId left join st_users on st_cars.userId=st_users.userId where st_car_requests.customerId='$userId'and st_car_requests.carRequestId = '$carRequestId' and st_assign_cars.status='active' and st_car_requests.status = 'approved' and st_cars.status = 'active' and st_users.status = 'active'";
 		return $this->db->query($sql)->result();
 	}

public function deleteAllRequest($carRequestId)
{
	$sql = "UPDATE st_car_requests set status = 'trash' where carRequestId='$carRequestId'";
	$sql1 = "UPDATE st_cars set inUse = 0 where carId IN (SELECT carId from st_assign_cars where carRequestId ='$carRequestId' and status='active')";
	$sql2 = "UPDATE st_assign_cars set status = 'trash' where carRequestId='$carRequestId'";	

	$this->db->trans_start(TRUE);
				$this->db->query($sql);
				$this->db->query($sql1);
				$this->db->query($sql2);
				
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
					{
        				$this->db->trans_rollback();
        				redirect('customerController/allRequest');
        				
					}
				else
					{
					    $this->db->trans_commit();
					    		redirect('customerController/allRequest');
					  
					}


}

//get the view event data from database(table)
 	public function viewEvent($userId)
 	{
 		$this->db->from('st_events');
 		$this->db->WHERE('userId',$userId);
 		$this->db->WHERE('status','active');
		$query = $this->db->get();
 		return $query->result();

 	}
 //Delete Event 
 	public function deleteViewEvent($eventId)
	{
		$data = array(
			'status' =>"trash",
			'adminStatus' => $this->session->userdata('fullNameAdmin'),
			 );
		$userIdAdmin = $this->session->userdata('userIdAdmin');
		$this->db->WHERE('eventId',$eventId);
		$this->db->update('st_events',$data);
		$this->session->set_flashdata('deleteViewEvent_message', 'Event Deleted successfully');
		redirect('customerController/viewEvent');
	}
//get the individual view event data from database(table)
 	public function individualViewEvent($eventId)
 	{
 		$this->db->from('st_events');
 		$this->db->where('eventId',$eventId);
		$query = $this->db->get();
 		return $query->row();

 	}

//get the individual view event data from database(IN update page)
 	public function getUpdateViewEvent($eventId)
 	{
 		$this->db->from('st_events');
 		$this->db->where('eventId',$eventId);
		$query = $this->db->get();
 		return $query->result();
 	}
 //final update the view event data 
 	public function updateViewEvent($data,$eventId)
 	{
 		$this->db->where('eventId', $eventId);
				$answer = $this->db->update('st_events', $data);

				if ($answer) {
					$this->session->set_flashdata('event_update_message', 'Update Successfully');
					redirect('customerController/updateViewEvent?id='.$eventId);	
					}
 	}

//car request 
 	public function carRequest($data)
 	{
 		$this->db->insert('st_car_requests',$data);
 		$this->session->set_flashdata('carRequest_message', 'Car Request Successfully');
 		redirect('customerController/carRequest');
 	}

//get all the request of car of individual user
 	public function getAllRequest($condition='')
 	{
 		// $sql = "SELECT * FROM st_car_requests ".$condition;

 		$sql = "SELECT sc.carRequestId,sc.noOfCars,sc.preferenceBranding,sc.startDate,sc.endDate,sc.status,count(st_assign_cars.assignId) as noOfAssignedCars 
 			from st_car_requests sc left JOIN st_assign_cars on st_assign_cars.carRequestId = sc.carRequestId and st_assign_cars.status = 'active'  ".$condition; 
 			
 		
 	 		
 		$query = $this->db->query($sql)->result();
 		
 		return $query;
 	}
 	public function updateAllRequest($data,$carRequestId)
 	{
 	
 		$this->db->where('carRequestId',$carRequestId);
 		$this->db->update('st_car_requests',$data);
 		$this->session->set_flashdata('updateAllRequest_message', 'Car Request Update Successfully');
 		redirect('customerController/updateAllRequest?id='.$carRequestId);

 	}
 	
public function viewOnMap($eventId)
{
	$this->db->select();
	$this->db->from('st_events');
	$this->db->where('eventId',$eventId);
	$query = $this->db->get();
	return $query->result();
}
public function noOfAssignedCars()
 	{
 		$sql = "SELECT count(st_assign_cars.assignId) as noOfAssignedCars from st_car_requests sc left JOIN st_assign_cars on st_assign_cars.carRequestId = sc.carRequestId and st_assign_cars.status = 'active' WHERE sc.customerId =".$this->session->userdata('userId')." AND (sc.status = 'approved' OR sc.status='pending')";
 		$query = $this->db->query($sql)->result_array();
 		return $query;

 	}


	// public function noOfassignCars()
	// {
	// 	$sql = "SELECT (select count(st_assign_cars.assignId) from st_car_requests left join st_assign_cars on st_assign_cars.carRequestId = st_car_requests.carRequestId where st_car_requests.customerId=su.userId and st_assign_cars.status = 'active') as totalCars from st_users su where su.status = 'active' and userType = 'customer' and userId = '98'";
	// 	$query = $this->db->query($sql)->result_array();
	// 	return $query[0]['totalCars'];
	// }
}
?>