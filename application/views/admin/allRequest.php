<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Request
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">All Request</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
              
          <!-- /.box -->
 
          <div class="box">
            <div class="box-header">
             <!--  <h4 class="box-title">View All Car Request</h4> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php if($this->session->flashdata('assignSelectedCar_message') != ''){?>
                  <div class="box-header with-border" >
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8"  style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('assignSelectedCar_message'); ?>
                  </div>
                  </div>
               <?php }?>
               <table  class="example1 table table-bordered table-striped">
                <thead>
                <tr>
                <th>Request By</th>
                  <th>No. of Car</th>
                  <th>Assigned Cars</th>
                  <th>Remaining Car</th>
                  <th>Preference Branding</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Status</th>
                 <!--  <th>approveDate</th>
                  <th>approveBy</th> -->
                  <th>Action</th>

                </tr>
                </thead>
                <tbody>


                <?php foreach ($result as $row) { ?>
                <?php 
$noOfCars = $row->noOfCars;
$noOfAssignedCars = $row->noOfAssignedCars;

$remainingCars = $row->noOfCars - $row->noOfAssignedCars;

?>
                <tr>
                <td> <?php echo  $row->fullName;?> </td>
                  <td> <?php echo  $row->noOfCars;echo($row->noOfCars == $row->noOfAssignedCars)? "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: green;' class='glyphicon glyphicon-ok-sign'></span>" : '' ?> </td>
                  <td><?php echo $row->noOfAssignedCars;?></td>
               <td><?php echo $remainingCars; ?></td>
                  <td> <?php echo $row->preferenceBranding;?> </td>
                  <td> <?php echo $row->startDate;?> </td>
                  <td> <?php echo $row->endDate;?> </td>
                  <td> <?php 

                     if ($row->scStatus == "approved") 
                      {
                          echo "<button type='button' class='btn btn-success btn-xs'>".$row->scStatus."</button>"; 
                      }
                      elseif($row->scStatus == "expired") 
                      {
                          echo "<button type='button' class='btn btn-danger btn-xs'>".$row->scStatus."</button>"; 
                      }
                      else
                      {
                          echo "<button type='button' class='btn btn-warning btn-xs'>".$row->scStatus."</button>" ;
                      }

                  ?> </td>

                  <!-- <td><?php echo $row->approveDate ?></td> -->

                 <!--  <td>
                  <?php 
                    if ($row->approveBy == "Pending Request") 
                  {
                    echo "<button type='button' class='btn btn-warning btn-xs'>".$row->approveBy."</button>";
                  }
                  else
                  {
                    echo "<button type='button' class='btn btn-success btn-xs'>".$row->approveBy."</button>";
                  }
                  ?>
                  </td> -->








                  <td>
                  <?php $customerId_array = array('customerId_session' => $row->customerId , );
                  $this->session->set_userdata($customerId_array); ?>
                 <!-- data-toggle="modal" data-target="#all_request_view_modal" for view the data -->
              
               <?php if ($noOfAssignedCars != 0) { ?>
              

               <a  href="<?php echo base_url('adminController/updateRequestApprovals?id='.$row->carRequestId);?>" type="button" class="btn btn-warning">Update</a>
              <?php } else{?>
               
         
                    <a  href="<?php echo base_url('adminController/requestApprovals?id='.$row->carRequestId);?>" type="button" class="btn btn-primary">Approve</a>

                    <?php }?>
          
                
             
                <a href="<?php echo base_url('adminController/allRequestDelete?id=').$row->carRequestId?>" type="button" class="btn btn-danger">Delete</a>
               </td>
                    
                  
                </tr>
                  
        <?php } ?>
                
               </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


  </div>

      



<!-- //modal -->
<div class="modal fade" id="all_request_view_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">View Request</h4>
        </div>
        <div class="modal-body">
           <table  class="example1 table table-bordered table-striped">
                <thead>
                <tr><th>No. of Car</th> <td> </td></tr>
                  <tr><th>Car Modal</th> <td> </td></tr>
                  <tr><th>Location</th> <td> </td></tr>
                  <tr><th>Date Range</th> <td> </td></tr>
                  <tr><th>Status</th> <td> </td></tr>
                  <tr><th>Action</th> <td> </td></tr>
                </thead>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>