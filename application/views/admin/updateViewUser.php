<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Client
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Update Client</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
         
 
        <a class="pull-right btn btn-primary" style="margin: 10px; " data-toggle="modal" data-target="#reset_view" href="#" class="btn btn-default btn-flat"><i class="fa fa-key" aria-hidden="true"></i>
 Reset Password</a>
        <!-- change Password Start -->
       <div class="modal fade" id="reset_view" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title"><i class="fa fa-key" aria-hidden="true"></i>
 Reset Password</h4>
        </div>
       
        <div class="modal-body">

         <div class="form-group">
            
                    <label for="exampleInputEmail1" >New Password</label>
              
                    <input autocomplete="off" required type="password" class="form-control" class="2" id="password1" name="npassword" placeholder="Enter New Password">
              
         </div> 
          
            <div class="form-group">
                 
                    <label for="exampleInputEmail1" >Confrim Password</label>
                  
                   <input autocomplete="off" required type="password" class="form-control" class="3" id="password2" name="cpassword" placeholder="Enter Confrim Password"><p id="validate-status11"></p>

          </div>

        </div>
        <div class="modal-footer">
          
           <button type="button" onclick="change_password_submit()" name="change_password_submit" class="btn btn-primary center-block"   >Confrim</button>
           <?php echo form_close();?>
        </div>
      </div>
      
    </div>
  </div>
        <!-- /////////////////change Password End -->
            <!-- /.box-header -->
            <!-- form start -->
           <?php echo form_open('adminController/updateViewUser_submit');?>

        
              <div class="box-body">
             <?php if($this->session->flashdata('updateViewUser_message') != ''){?>
                  <div class="box-header with-border" >
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8"  style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('updateViewUser_message'); ?>
                  </div>
                  </div>
               <?php }?>
               
              <div class="col-md-1"></div>

              <div class="col-md-8">


<?php foreach ($result as $row) {

               
                  $userId = $row->userId;
                  $fullName = $row->fullName; 
                  $userName = $row->userName; 
                   $companyRegistrationNumber = $row->companyRegistrationNumber; 
                  $address = $row->address; 
                  $email = $row->email; 
                   $since = $row->since; 
                   $phone = $row->phone; 
                 ?>
              
<input type="hidden" name="userId" value="<?php echo $userId?>">
                
                           
            
   <!--           <div class="form-group">
              
                 
                  <img style="width: 15%;height: 83px;" src=<?php #echo base_url('assets/uploads/images')."/".$userImage?>>
                  <input type="file" name="userImage" value="<?php #echo $userImage?>">
               </div>
 -->

                <div class="form-group">
                  <label for="labelUserName" >User Name</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputUserName" name="userName" value="<?php echo $userName ?>">
               </div>

               <div class="form-group">
                  <label for="labelPassport" >Email Adress</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputEmail" name="email" value="<?php echo $email ?>">
               </div>

              
                <div class="form-group">
                  <label for="labelFullName" >Client Name</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputFullName" name="fullName" value="<?php echo $fullName ?>">
                </div>

                
                <div class="form-group">
                  <label for="labelPhone" >Phone</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputPhone" name="phone" value="<?php echo $phone ?>">
                </div>
             

                  <div class="form-group">
                  <label for="labelAdress" >Address</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputAdress" name="address" value="<?php echo $address ?>">
               </div>


                 <div class="form-group">
                  <label for="labelCompanyRegistrationNumber" >Company Registration Number</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputCompanyRegistrationNumber" name="companyRegistrationNumber" value="<?php echo $companyRegistrationNumber ?>">
               </div>

                


                 <div class="form-group">
                  <label for="labelsince" >Since</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputsince" name="since" value="<?php echo $since ?>">
               </div>

                 

                


               
              
            <div class="box-footer">
                <button type="submit" class="btn btn-primary center-block" name="updateViewUser_submit">Update</button>
              </div>
                
              </div>  <!-- col-md-6 -->
              </div><!-- /.box-body -->

             
            <?php echo form_close();?>
            </div>
          <!-- /.box -->

       <?php }?> <!-- end of for loop -->  
      

        </div>
        <!--/.col (left) -->
   
        
     
      </div>
      <!-- /.row -->
      </section>
      </div>
   

   
