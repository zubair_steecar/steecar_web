
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title; ?>       
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active"><?php echo $breadcrumb; ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12 col-md-12">
    <!-- /.box -->

                      
             
          <div class="box box-primary">
               <?php if($this->session->flashdata('client_Delete_success') != ''){?>
                  <div class="box-header with-border" >
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8"  style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('client_Delete_success'); ?>
                  </div>
                  </div>
               <?php }?>

                <!-- Delete mesage -->
                 <?php if($this->session->flashdata('client_Delete_error') != ''){?>
               <div class="box-header with-border" >   
                 <div class="col-md-1"></div>
                   <div class="alert alert-danger alert-dismissable col-md-8"  style="background: #ec9d93 !important; color: red !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('client_Delete_error'); ?>
                  </div>
                  </div>
               <?php }?>
            <div class="box-header with-border">
             <!--  <h3 class="box-title">Data Table of Registered User</h3> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <table class="table table-bordered table-striped example1">
                <thead>
                <tr>
                  <!-- <th>Image</th> -->
                  <th>Client Name</th>
                  <th>User Name</th>
                  <th>Email Address</th>
                  <th>Phone</th>
                  <th>Assigned Cars</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($result as $row) { ?>
                 <tr>
<!-- 
                  <td style="width: 10%"><img style=" width: 76%; height: 6%;" src=<?php echo base_url().'assets/uploads/images/'.$row->userImage ?>></td> -->
                  <td><?php echo $row->fullName ?> </td>
                  <td><?php echo $row->userName ?></td>
                  <td><?php echo $row->email ?></td>
                  <td><?php echo $row->phone ?></td>
                  <?php if ($row->totalCars == 0 ) { ?>
                     <td><?php echo $row->totalCars ?> </td>
                  <?php }
                  else {?>
                  <td><b><a href="<?php echo base_url().'adminController/userCars/'.$row->userId; ?>"> <?php echo $row->totalCars ?>  <i class="fa fa-external-link"></i></a></b></td>
                  <?php } ?>
                  <td>
                     <a onclick="viewClient(<?php echo $row->userId;?>)" class="btn btn-primary"> View</a>
                    <a  class="btn btn-success" href= <?php echo base_url('adminController/updateViewUser?id=').$row->userId;?> >Update</a>
                    <a class="btn btn-danger" href= <?php echo base_url('adminController/deleteViewUser?id=').$row->userId;;?> >Delete</a>
                  </td>
                </tr>
                <?php } ?>
                
              
                </tbody>
             
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


  </div>

   


