<!DOCTYPE html>
<html>
  <head>
    <title>Place Autocomplete</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    
 
  </head>
  <body>
  <div>

   <div >
  <input id="autocomplete" placeholder="Enter your address"
         onFocus="geolocate()" type="text"></input>
</div>

<table>
  <tr>
    <td>Street address</td>
    <td><input id="street_number"
          disabled="true"></input></td>
    <td colspan="2"><input id="route"
          disabled="true"></input></td>
  </tr>
  <tr>
    <td>City</td>
    <!-- Note: Selection of address components in this example is typical.
         You may need to adjust it for the locations relevant to your app. See
         https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete-addressform
    -->
    <td colspan="3"><input id="locality"
          disabled="true"></input></td>
  </tr>
  <tr>
    <td>State</td>
    <td><input
          id="administrative_area_level_1" disabled="true"></input></td>
    <td>Zip code</td>
    <td <input id="postal_code"
          disabled="true"></input></td>
  </tr>
  <tr>
    <td>Country</td>
    <td colspan="3"><input id="country" disabled="true"></input></td>
  </tr>
</table>

</div>
<!-- Replace the value of the key parameter with your own API key. -->

  </body>
</html>



<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_MAP_API_KEY ?>&libraries=places&callback=initAutocomplete" async defer></script>

 <script>
var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
      {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
    </script>