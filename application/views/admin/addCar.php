<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Driver
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Add Driver</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <?php if($this->session->flashdata('message') != ''){?>
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8"  style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('message'); ?>
                  </div> <br>
                  
               <?php }?>
              
              <!-- Inserted Car Successfully  -->
                <?php if($this->session->flashdata('message') != ''){?>
                  
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8"  style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('message'); ?>
                  </div>
                  
               <?php }?>
              <!-- edit -->
              <!-- for no email found -->
              
               <?php if($this->session->flashdata('no_email_message') != ''){?>
               <br>
                   <div class="alert alert-danger alert-dismissable col-md-4">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('no_email_message'); ?>
                  </div>
               <?php }?>
                <!-- for no UserName found -->
              <?php if($this->session->flashdata('no_userName_message') != ''){?>
              <br>
                   <div class="alert alert-danger alert-dismissable col-md-4">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('no_userName_message'); ?>
                  </div>
               <?php }?>

            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
          
            <!-- <div class="box-footer">
                <h3>Add New Driver</h3>
              </div>
              -->
<?php echo form_open_multipart('adminController/addCar');?>
 <input type="hidden" name="userType" value="driver">
              <div class="box-body">
             
            <div id="newD">
            <div class="row">
              <div class="col-md-6">
              
            
            <div class="form-group">
                  <label for="labelUserName" >User Name</label>
                  <input autocomplete="off" required type="text" class="form-control" id="userName" name="userName" placeholder="Enter User Name" onBlur="checkUserNameAjax()">
                  <span id="user-availability-status"></span>
            </div>
            
            <div class="form-group">
                  <label for="labelFullName" >Driver Name</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputFullName" name="fullName" placeholder="Enter Full Name">
            </div>
             <div class="form-group">
                  <label for="labelEmail" >Email</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputEmail" name="email" placeholder="Enter Email">
            </div>

            <div class="form-group">
                  <label for="labelFatherName" >Father Name</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputFatherName" name="fatherName" placeholder="Enter Father Name">
            </div>
            <div class="form-group">
                 <label for="labelGander" > Select Gender</label>
                 <br>
                  <input type="radio" value="male" name="gender"> Male
                   <input type="radio" value="female" name="gender" > Female
            </div>

            <div class="form-group">
                  <label for="labelPhone" >Phone</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputPhone" name="phone" placeholder="Enter Phone">
            </div>

            <div class="form-group">
                  <label for="labelAddress" >Address</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputAddress" name="address" placeholder="Enter Address">
            </div>

           
              
            <div class="form-group">
                 <label for="InputCnic" >CNIC Number</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputCnic" name="cnic" placeholder="Enter CNIC Number">
            </div>

            <div class="form-group">
                 <label for="labelLicense" >License Number</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputLicense" name="license" placeholder="Enter License Number">
            </div>

            <div class="form-group">
                 <label for="labelAge" >Age</label>
                  <input autocomplete="off" required type="number" class="form-control" id="inputAge" name="age" placeholder="Enter Age">
            </div>
                
            <div class="form-group">
                  <label for="labelPassword" >Password</label>
                  <input autocomplete="off" required type="password" class="form-control" id="inputpassword" name="password" placeholder="Password">
            </div>

            <div class="form-group">
                  <label for="labelFile" >Image</label>
                  <input autocomplete="off" type="file" id="inputFile" name="file_name">
            </div>
                
                 
             </div>   <!-- col-md-6 end -->

              <!-- //car details -->
             <div class="col-md-6">
         
              <div class="form-group">
                    <label for="labelCarModel" >Car Model</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputCarModel" name="carModel" placeholder="Enter Car Model">
              </div>

              <div class="form-group">
                  <label for="labelRegistrationNumber" >Registration Number</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputRegistrationNumber" name="registrationNumber" placeholder="Enter Registration Number">
              </div>

               
              <div class="form-group">
                 <label >Color</label>
                 <select name="color" class="form-control select2" style="width: 100%;">
                 <option value="">Select Color</option>
<option value="Black" style="background-color: Black;color: #FFFFFF;">Black</option>
<option value="Gray" style="background-color: Gray;">Gray</option>
<option value="DarkGray" style="background-color: DarkGray;">DarkGray</option>
<option value="LightGray" style="background-color: LightGrey;">LightGray</option>
<option value="White" style="background-color: White;">White</option>
<option value="Aquamarine" style="background-color: Aquamarine;">Aquamarine</option>
<option value="Blue" style="background-color: Blue;">Blue</option>
<option value="Navy" style="background-color: Navy;color: #FFFFFF;">Navy</option>
<option value="Purple" style="background-color: Purple;color: #FFFFFF;">Purple</option>
<option value="DeepPink" style="background-color: DeepPink;">DeepPink</option>
<option value="Violet" style="background-color: Violet;">Violet</option>
<option value="Pink" style="background-color: Pink;">Pink</option>
<option value="DarkGreen" style="background-color: DarkGreen;color: #FFFFFF;">DarkGreen</option>
<option value="Green" style="background-color: Green;color: #FFFFFF;">Green</option>
<option value="YellowGreen" style="background-color: YellowGreen;">YellowGreen</option>
<option value="Yellow" style="background-color: Yellow;">Yellow</option>
<option value="Orange" style="background-color: Orange;">Orange</option>
<option value="Red" style="background-color: Red;">Red</option>
<option value="Brown" style="background-color: Brown;">Brown</option>
<option value="BurlyWood" style="background-color: BurlyWood;">BurlyWood</option>
<option value="Beige" style="background-color: Beige;">Beige</option>
                </select>
              
              </div>
                
              <div class="form-group">
                  <label for="labelPrefrenceBranding" >Prefrence Branding</label> <br>
                  <input type="radio" value="full" name="prefrenceBranding" id="exampleInputprefrenceBranding"> Full
                  <input type="radio" value="partial" name="prefrenceBranding" > Partial
              </div>

              <div class="form-group">
                  <label for="labelManufacturer" >Manufacturer</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputManufacturer" name="manufacturer" placeholder="Enter Manufacturer ">
              </div>

              <div class="form-group">
                  <label for="labeltrackerId" >Tracking ID</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputtrackerId" name="trackerId" placeholder="Enter Tracking ID ">
              </div>
             
           </div> <!-- col-md-6 end -->

                 
              
          

             </div>   <!-- row end -->
             <div class="row">
               <div class="col-md-12">
                    <div class="box-footer">
                         <button type="submit" class="btn btn-primary center-block" name="add_car_submit">Add Driver</button>
                    </div>
               </div>
             </div>
             <?php echo form_close();?>
          </div> <!-- neD end -->
         
          <!-- /.box -->

       
  
           
          
            <!-- form start -->
          
         </div>

      
<!-- ////////////////////////////////dropdown end -->

        </div>
        <!--/.col (left) -->
   
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
   
    <!-- /.Tables -->
   
    
    </section>
    </div>