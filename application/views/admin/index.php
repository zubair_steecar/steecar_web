 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $totalUser?></h3>

              <p>Clients</p>
            </div>
            <div class="icon">
              <i class="fa fa-user" aria-hidden="true"></i>
            </div>
            <a href="<?php echo base_url('adminController/allUser');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>


        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $totalDrivers?></h3>

              <p>Drivers</p>
            </div>
            <div class="icon">
              <i class="fa fa-car" aria-hidden="true"></i>
            </div>
            <a href="<?php echo base_url('adminController/allCar');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $totalEvents?></h3>

              <p>Events</p>
            </div>
            <div class="icon">
              <i class="fa fa-calendar" aria-hidden="true"></i>
            </div>
            <a href="<?php echo base_url('adminController/viewEvent');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

          <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $totalRquests?></h3>

              <p>Request</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo base_url('adminController/allRequest');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

        
      </div>

   
     

         <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-xs-12">
      
          <!-- /.box -->
 
          <div class="box">
            <div class="box-header">
              <h4 class="box-title">Events</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table id="" class="table table-bordered table-striped example1">
                <thead>
                <tr>
                  <th>Event Name</th>
                  <th>Location</th>
                  <th>Latitude</th>
                  <th>Longitude</th>
                  <th>Event Date</th>
                  <th>Event Time</th>
                  
                  <!-- <th>Created By</th> -->
                  <th>Status</th>
                  <th style="width: 320px !important ;">Action</th>
                  

                </tr>
                </thead>
                <tbody>
             <?php foreach ($result as $row) { ?> 
                <tr>
                  <td> <?php echo $row->eventName?> </td>
                  <td> <?php echo $row->eventLocation?> </td>
                  <td> <?php echo $row->locLatitude?> </td>
                  <td> <?php echo $row->locLongitude?> </td>
                  <td> <?php echo $row->eventDate?> </td>
                  <td> <?php echo $row->eventTime?> </td>
               
                 <!-- <td> <?php #echo $row->userId?> </td> -->
                  <td> 

                   <?php 
                     

                  
                      if ($row->status == "active") 
                      {
                          echo "<button type='button' class='btn btn-success btn-xs'>".$row->status."</button>"; 
                      }
                      elseif($row->status == "trash" || $row->status == "cancel") 
                      {
                          echo "<button type='button' class='btn btn-danger btn-xs'>".$row->status."</button>"; 
                      }
                      else
                      {
                      
                          echo "<button type='button' class='btn btn-warning btn-xs'>".$row->status."</button>" ;
                      }
                   
                 ?> 

                  </td>
                   <td>
                 
                  <a onclick="viewEvent(<?php echo $row->eventId;?>)" class="btn btn-primary">View</a>
                  <a href=<?php echo base_url('adminController/updateViewEvent?id=').$row->eventId;?> class="btn btn-success">Update</a>
                  <a href=<?php echo base_url('adminController/deleteViewEvent?event=').$row->eventId;?> class="btn btn-danger">Delete</a>
                   <a href=<?php echo base_url('adminController/viewOnMap?event=').$row->eventId;?> class="btn btn-warning">View on Map</a>
             
                  
                </tr>
                </td>
                  
           <?php } ?>
                
               </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      
      <!-- /.row -->
    </section>

<!-- //car details page  -->
      <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-xs-12">
      
          <!-- /.box -->
 
          <div class="box">
            <div class="box-header">
              <h4 class="box-title">Clients</h4>
            </div>
              <!-- /.box-header -->
            <div class="box-body">
             <table id="" class="table table-bordered table-striped example1">
                <thead>
                <tr>
                  <!-- <th>Image</th> -->
                  <th>Client Name</th>
                  <th>User Name</th>
                  <th>Email Address</th>
                  <th>Phone</th>
                  <th>Assigned Cars</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($UserResult as $row) { ?>
                 <tr>
<!-- 
                  <td style="width: 10%"><img style=" width: 76%; height: 6%;" src=<?php echo base_url().'assets/uploads/images/'.$row->userImage ?>></td> -->
                  <td><?php echo $row->fullName ?> </td>
                  <td><?php echo $row->userName ?></td>
                  <td><?php echo $row->email ?></td>
                  <td><?php echo $row->phone ?></td>
                  <?php if ($row->totalCars == 0 ) { ?>
                     <td><?php echo $row->totalCars ?> </td>
                  <?php }
                  else {?>
                  <td><b><a href="<?php echo base_url().'adminController/userCars/'.$row->userId; ?>"> <?php echo $row->totalCars ?>  <i class="fa fa-external-link"></i></a></b></td>
                  <?php } ?>
                  <td>
                     <a onclick="viewClient(<?php echo $row->userId;?>)" class="btn btn-primary btn-sm"> View</a>
                    <a  class="btn btn-success" href= <?php echo base_url('adminController/updateViewUser?id=').$row->userId;?> >Update</a>
                    <a class="btn btn-danger" href= <?php echo base_url('adminController/deleteViewUser?id=').$row->userId;;?> >Delete</a>
                  </td>
                </tr>
                <?php } ?>
                
              
                </tbody>
             
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      
      <!-- /.row -->
    </section>
<!-- end car detail page -->


</section>
 </div>