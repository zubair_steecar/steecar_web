 <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>   


    <!-- /.Tables -->
 
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Working Location
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">View Working Location</li>
      </ol>
    </section>
      <section class="content">
      <div class="row">
        <div class="col-xs-12">
      
         
 
          <div class="box box-primary">
            
              <!-- <h3 class="box-title">View All Registered Areas</h3> -->
               <?php if($this->session->flashdata('updateWorkingLocation_message') != ''){?>
                  <div class="box-header with-border" >
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8"  style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('updateWorkingLocation_message'); ?>
                  </div>
                  </div>
               <?php }?>

                <!-- Delete mesage -->
                 <?php if($this->session->flashdata('deleteWorkingLocation_message') != ''){?>
               <div class="box-header with-border" >   
                 <div class="col-md-1"></div>
                   <div class="alert alert-danger alert-dismissable col-md-8"  style="background: #ec9d93 !important; color: red !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('deleteWorkingLocation_message'); ?>
                  </div>
                  </div>
               <?php }?>

          
            <!-- /.box-header -->
            <div class="box-body">
               <table class="table table-bordered table-striped example1">
                <thead>
                <tr>
                  <th>Id</th>
                  <!-- <th>City</th> -->
                  <th>Area</th>
                  <th>Latitude</th>
                  <th>Longitude</th>                  
                  <th>Option</th>

                </tr>
                </thead>
                <tbody>
                <?php foreach ($result as $row) { ?>
                <tr>
                  <td><?php echo $row->locationId?></td>
                  <!-- <td><?php #echo $row->city?></td> -->
                  <td><?php echo $row->locationText?></td>
                  <td><?php echo $row->latitude?></td>
                  <td><?php echo $row->longitude?></td>                  
                  <td>
                 
                  <a  class="btn btn-success" href="<?php echo base_url('adminController/updateWorkingLocation?id=').$row->locationId;?>">Update</a>
                  <a class="btn btn-danger" href="<?php echo base_url('adminController/deleteWorkingLocation?id=').$row->locationId;?>">Delete</a>
                  </td>
                  
                </tr>
                  
                <?php }?>
                
               </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
