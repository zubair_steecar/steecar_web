<link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>css/mapStyle.css">    
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Event
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Update Event</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
        
            <!-- /.box-header -->
            <!-- form start -->
           <?php echo form_open('adminController/updateViewEvent');?>

        
              <div class="box-body">
             
              <div class="col-md-1"></div>

              <div class="col-md-8">

<div class="box-header with-border" >
                      <?php if($this->session->flashdata('event_update_message') != ''){?>
                  
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8"  style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('event_update_message'); ?>
                  </div>
                  
               <?php }?>
               </div>
      
              
<?php foreach ($result as $row) {?>
             <input autocomplete="off" required type="hidden" class="form-control" id="inputeventId" name="eventId" value=<?php echo $row->eventId ?>>

              <div class="form-group">
                  <label for="labelEventName" >Event Name</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputEventName" name="eventName" value="<?php echo $row->eventName ?>">
                  </div>
             
                   <div id="pac-container">
      <div class="form-group">
      <label for="labelNoOfCar" >Event Location</label>
        <input name="eventLocation" class="form-control" id="pac-input" type="text"
            placeholder="Enter a location"  value="<?php echo $row->eventLocation ?>">
            </div>
             </div>

    <div id="map" style="height: 300px;margin: 0px;padding: 0px;"></div>
    <div id="infowindow-content">
      <img src="" width="16" height="16" id="place-icon">
      <span id="place-name"  class="title"></span><br>
      <span id="place-address"></span>
    </div>



                 <div class="form-group">
                  <label for="labelLatitude" >Latitude</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputLatitude" name="locLatitude" value="<?php echo $row->locLatitude ?>">
               </div>
                 <div class="form-group">
                  <label for="labelLongitude" >Longitude</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputLongitude" name="locLongitude"  value="<?php echo $row->locLongitude ?>">
               </div>

               

                   <!-- Date range -->
                <div class="form-group">
                <label>Event Date:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="eventDate" id="Startdatepicker" autocomplete="off" required value="<?php echo $row->eventDate ?>">
                </div>
                <!-- /.input group -->
              </div>

               <div class="bootstrap-timepicker">
                <div class="form-group">
                  <label>Event Time </label>

                  <div class="input-group">
                    <input type="text" class="form-control timepicker" name="eventTime" autocomplete="off" required value="<?php echo $row->eventTime ?>">

                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>


                
<?php }?>
              
            <div class="box-footer">
                <button type="submit" class="btn btn-primary center-block" name="updateViewEvent_submit">Update</button>
              </div>
                
              </div>  <!-- col-md-6 -->
              </div><!-- /.box-body -->

             
            <?php echo form_close();?>
            </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
      

        </div>
        <!--/.col (left) -->
   
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
      </section>
      </div>
   
   



   <!-- AutoCompletMap -->
<script src="<?php echo base_url().'assets/'; ?>js/autoCompleteMapUpdate.js"></script>

   

