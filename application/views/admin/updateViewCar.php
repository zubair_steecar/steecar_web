<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Driver
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Update Driver</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
             
          <div class="box box-primary">
         <div class="box-header with-border" >
         </div>
            <!-- /.box-header -->
            <!-- form start --><?php if($this->session->flashdata('updateCar_message') != ''){?>
                <div class="box-header with-border" >
                 <div class="col-md-1"></div>
                   <div style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message"  class="alert alert-success alert-dismissable col-md-7">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('updateCar_message'); ?>
                  </div>
                  </div>
                  
               <?php }?>
    


              <div class="box-body">
             
      

        

                      <!-- /////////////////***UPDATE FORM START ****///////////// -->

                             

                              <?php echo form_open_multipart('adminController/updateViewCar');?>  <!-- //add car me update hoga final -->
         
           <?php foreach ($result as $row) {

               
                  $userId = $row->userId;
                  $carId = $row->carId;
                  $fullName = $row->fullName; 
                  $userName = $row->userName; 
                  $fatherName = $row->fatherName; 
                  $address = $row->address; 
                  $email = $row->email; 
                  $age = $row->age; 
                  $gender = $row->gender;   
                  $cnic = $row->cnic; 
                  $phone = $row->phone; 
                  $userImage = $row->userImage ;
                  $license = $row->license;

                  $carModel = $row->carModel;   
                  $registrationNumber = $row->registrationNumber; 
                  $color = $row->color; 
                  $preferenceBranding = $row->preferenceBranding ;
                  $manufacturer = $row->manufacturer;
                  $trackerId = $row->trackerId;


                   }?>
                  <input type="hidden" name="carId" value="<?php echo $carId?>">
                <input type="hidden" name="userId" value="<?php echo $userId?>">
                <!-- <input type="hidden" name="userType" value="<?php #echo $userType?>"> -->
                  <div class="col-md-6">
                <div class="form-group">
                  <label for="labelUserName" >User Name</label>
                  <input type="text" class="form-control" id="inputUserName" value="<?php echo $userName ?>" name="userName" placeholder="Enter User Name">
                </div>
         
               <div class="form-group">
                  <label for="labelFullName" >Driver Name</label>
                  <input type="text" class="form-control" id="inputFullName" value="<?php echo $fullName ?>" name="fullName" placeholder="Enter Full Name">
                </div>

                <div class="form-group">
                  
                  <label for="labelEmail" >Email</label>
                  <input type="text" class="form-control" id="inputEmail" value="<?php echo $email ?>" name="email" placeholder="Enter Email">
              
                </div>
         
                  <div class="form-group">
                  
                  <label for="labelFatherName" >Father Name</label>
                  <input type="text" class="form-control" id="inputFatherName" value="<?php echo $fatherName ?>" name="fatherName" placeholder="Enter Father Name">
                </div>
          
                 <div class="form-group">
                 
                 <label for="labelGender" > Select Gender</label>
                 <br>
                 <?php if ($gender == "male") {?>
                    <input type="radio" value="male" name="gender" checked> Male
                    <input type="radio" value="female" name="gender" > Female
                <?php }
                else{?>
                    <input type="radio" value="male" name="gender"> Male
                   <input type="radio" value="female" name="gender" checked> Female
                   <?php } ?>
                </div>
             
                  <div class="form-group">
                  
                  <label for="labelPhone" >Phone</label>
                  <input type="text" class="form-control" id="inputPhone" value="<?php echo $phone ?>" name="phone" placeholder="Enter Phone">
                </div>
            
                <div class="form-group">
                
                  <label for="labelAddress" >Address</label>
                  <input type="text" class="form-control" id="inputAddress" value="<?php echo $address ?>" name="address" placeholder="Enter Address">
                </div>
         
                  
              
                 <div class="form-group">
                 
                  <label for="labelCnic" >CNIC Number</label>
                  <input type="text" class="form-control" id="inputCnic" value="<?php echo $cnic ?>" name="cnic" placeholder="Enter CNIC Number">
                </div>
         
                <div class="form-group">
                
                  <label for="labelPassport" >License Number</label>
                  <input type="text" class="form-control" id="inputPassport" value="<?php echo $license ?>" name="license" placeholder="Enter license Number">
                </div>
              
                 <div class="form-group">
                 
                  <label for="labelAge" >Age</label>
                  <input type="number" class="form-control" id="inputAge" value="<?php echo $age?>" name="age" placeholder="Enter Age">
                </div>
            </div>
              <!--  
                <div class="form-group">
                
                  <label for="labelFile" >Image</label>
                  <input type="file" id="inputFile" value="<?php #echo $userImage ?>" name="file_name">
                  </div>
                  -->

                  <!-- //car details -->

                  <div class="col-md-6">
                    <div class="form-group">
                    
                  <label for="labelCarModel" >Car Model</label>
                  <input type="text" class="form-control" id="inputCarModel" value="<?php echo $carModel ?>" name="carModel" placeholder="Enter Car Model">
                </div>
                
                  <div class="form-group">
                  
                  <label for="labelRegistrationNumber" >Registration Number</label>
                  <input type="text" class="form-control" id="inputRegistrationNumber" value="<?php echo $registrationNumber?>" name="registrationNumber" placeholder="Enter Registration Number">
                </div>
               

               
                 <div class="form-group">
                 
                <label >Color</label>
                
                <select name="color" class="form-control select2" style="width: 100%;">
                 <option value="<?php echo $color?>"><?php echo $color?></option>
<option value="Black" style="background-color: Black;color: #FFFFFF;">Black</option>
<option value="Gray" style="background-color: Gray;">Gray</option>
<option value="DarkGray" style="background-color: DarkGray;">DarkGray</option>
<option value="LightGray" style="background-color: LightGrey;">LightGray</option>
<option value="White" style="background-color: White;">White</option>
<option value="Aquamarine" style="background-color: Aquamarine;">Aquamarine</option>
<option value="Blue" style="background-color: Blue;">Blue</option>
<option value="Navy" style="background-color: Navy;color: #FFFFFF;">Navy</option>
<option value="Purple" style="background-color: Purple;color: #FFFFFF;">Purple</option>
<option value="DeepPink" style="background-color: DeepPink;">DeepPink</option>
<option value="Violet" style="background-color: Violet;">Violet</option>
<option value="Pink" style="background-color: Pink;">Pink</option>
<option value="DarkGreen" style="background-color: DarkGreen;color: #FFFFFF;">DarkGreen</option>
<option value="Green" style="background-color: Green;color: #FFFFFF;">Green</option>
<option value="YellowGreen" style="background-color: YellowGreen;">YellowGreen</option>
<option value="Yellow" style="background-color: Yellow;">Yellow</option>
<option value="Orange" style="background-color: Orange;">Orange</option>
<option value="Red" style="background-color: Red;">Red</option>
<option value="Brown" style="background-color: Brown;">Brown</option>
<option value="BurlyWood" style="background-color: BurlyWood;">BurlyWood</option>
<option value="Beige" style="background-color: Beige;">Beige</option>
                </select>
              </div>
             
                
                
                  <div class="form-group">
                  
                  <label for="labelPrefrenceBranding" >Prefrence Branding</label><br>
                <?php if ($preferenceBranding == "full") {?>
                  <input type="radio" value="full" name="prefrenceBranding" checked> Full
                   <input type="radio" value="partial" name="prefrenceBranding" > Partial
               <?php }
               else{?>
                  <input type="radio" value="full" name="prefrenceBranding" > Full
                   <input type="radio" value="partial" name="prefrenceBranding" checked> Partial
                   <?php } ?>
                </div>
                
                  <div class="form-group">
                  
                  <label for="labelmanufacturer" >Manufacturer</label>
                  <input type="text" class="form-control" id="inputmanufacturer" value="<?php echo $manufacturer?>" name="manufacturer" placeholder="Enter Manufacturer">
                </div>

                 <div class="form-group">
                  <label for="labeltrackerId" >Tracking ID</label>
                  <input autocomplete="off" required type="text" class="form-control"  value="<?php echo $trackerId?>" id="inputtrackerId" name="trackerId" placeholder="Enter Tracking ID ">
              </div>
              </div>

         
                
              


                              <!-- ///////////////****UPDATE FORM END/////////////////// -->
                          
              
           
                
              </div>  <!-- col-md-6 -->
               <div class="box-footer">
                <button type="submit" class="btn btn-primary center-block" name="updateCarSubmit">Update</button>
              </div>
              </div><!-- /.box-body -->
              
            <?php echo form_close();?>
            </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
      

        </div>
        <!--/.col (left) -->
   
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
      </section>
      </div>
   

   

