<link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>css/mapStyle.css">    <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Working Location
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Update Working Location</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
        
            <!-- /.box-header -->
            <!-- form start -->
           <?php echo form_open('adminController/updateWorkingLocation');?>

           <?php foreach ($result as $row) {
                $locationId = $row->locationId;
               $locationText = $row->locationText;
               $longitude = $row->longitude;
               $latitude = $row->latitude;
           }?>
              <div class="box-body">
             
              <div class="col-md-1"></div>

              <div class="col-md-8">

       <!--         <div class="form-group">
                  <label for="labelLocationId" >Location Id</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputLocationId" name="locationId" value="<?php #echo $locationId ?>">
               </div> -->
               <input autocomplete="off" required type="hidden" class="form-control" id="inputLocationId" name="locationId" value="<?php echo $locationId ?>">

        

                 <div id="pac-container">
      <div class="form-group">
      <label for="labelNoOfCar" >Area</label>
        <input name="locationText" class="form-control" id="pac-input" type="text" value="<?php echo $locationText ?>">
            </div>
             </div>

    <div id="map" style="height: 300px;margin: 0px;padding: 0px;"></div>
    <div id="infowindow-content">
      <img src="" width="16" height="16" id="place-icon">
      <span id="place-name"  class="title"></span><br>
      <span id="place-address"></span>
    </div>



                 <div class="form-group">
                  <label for="labelLatitude" >Latitude</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputLatitude" name="latitude" value="<?php echo $latitude ?>">
               </div>
                 <div class="form-group">
                  <label for="labelLongitude" >Longitude</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputLongitude" name="longitude"  value="<?php echo $longitude ?>">
               </div>

             

              
              
            <div class="box-footer">
                <button type="submit" class="btn btn-primary center-block" name="updateWorkingLocation_submit">Update</button>
              </div>
                
              </div>  <!-- col-md-6 -->
              </div><!-- /.box-body -->

             
            <?php echo form_close();?>
            </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
      

        </div>
        <!--/.col (left) -->
   
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
      </section>
      </div>
   

   

   <!-- AutoCompletMap -->
<script src="<?php echo base_url().'assets/'; ?>js/autoCompleteMapUpdate.js"></script>

