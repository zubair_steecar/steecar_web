 <div class="content-wrapper">
    <!-- Main content -->
     <section class="content-header">
      <h1>
        <?php echo $title; ?>  </h1>
   
  
<p><img src="https://yalantis-com-dev-06-09.s3.amazonaws.com/uploads/ckeditor/pictures/365/content_Loading-Loop-1.gif" id="loaderIcon" style="display:none" </span></p>
     
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active"><?php echo $breadcrumb ?></li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
     
          <!-- /.box -->

          <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <h3 class="box-title">Registered User With Car Data</h3> -->
            </div>
            <!-- /.box-header -->
            <!-- <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet"> -->
           
            <div class="box-body">
            <!--   <a  class="btn btn-primary pull-right" href="<?php #echo base_url().'adminController/addCar'; ?>"> Add Car</a> -->
              <table class="table table-bordered table-striped example1">

                <thead>
                <tr>
                  <!-- <th>Image</th> -->
               
                  <th>Driver Name</th>
                 
                  <th>Phone</th>
                  <th>Car Model</th>
                  <th>Registration #</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($all_result as $row) { ?>
                <tr>
                  <!-- <td style="width: 10%"><img class="img img-responsive" alt="user Image" style=" width: 76%; height: 6%;" src=<?php echo base_url().'assets/uploads/images/'.$row->userImage ?>></td> -->
                 
                  <td><?php echo $row->fullName?></td>
                  
                  <td><?php echo $row->phone?></td>
                  <td><?php echo $row->carModel?></td>
                  <td><?php echo $row->registrationNumber?></td>
                  <td>
                  <!-- href=<?php #echo base_url().'adminController/individual_viewCar?id='.?>  -->
                
<a onclick="view_car_data(<?php echo $row->carId;?>)"  class="btn  btn-primary">View</a>
          <!-- <a href=<?php #echo base_url().'adminController/#?id='.$row->carId?>><i class="fa fa-eye" aria-hidden="true"></i></a> -->
                                          <!-- view Modal  -->
                     
 <a href=<?php echo base_url().'adminController/updateViewCar?id='.$row->carId?> class="btn btn-success">Update</a>
                  <!-- <a href=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i></i></a> -->
                                                   
                        <!-- DELETE  -->
                        <a href=<?php echo base_url().'adminController/deleteAllCar?id='.$row->carId.'&redirect='.$redirect ?> class="btn btn-danger">Delete</a>
                   
                  </td>
                  
                </tr>
                  
               <?php }?>
                
                
               
                </tbody>
              
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


  </div>







                  
         