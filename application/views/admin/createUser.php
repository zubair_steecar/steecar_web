<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create Client         
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Create Client</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- <div class="col-md-3"></div> -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
             
                     <!-- Inserted Area Successfully  -->
                <?php if($this->session->flashdata('create_user_message') != ''){?>
                <br>
                 <div class="col-md-1"></div>
                   <div style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message"  class="alert alert-success alert-dismissable col-md-7">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('create_user_message'); ?>
                  </div>
               <?php }?>
               <!-- for no email found -->
              
               <?php if($this->session->flashdata('no_email_message_user') != ''){?>
               <br>
                <div class="col-md-1"></div>
                   <div class="alert alert-danger alert-dismissable col-md-4">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('no_email_message_user'); ?>
                  </div>
               <?php }?>
                <!-- for no UserName found -->
              <?php if($this->session->flashdata('no_userName_message_user') != ''){?>
              <br>
               <div class="col-md-1"></div>
                   <div class="alert alert-danger alert-dismissable col-md-7">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('no_userName_message_user'); ?>
                  </div>
               <?php }?>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('adminController/createUser');?>
            
            <input type="hidden" value="customer" name="userType">
              <div class="box-body">
              <div class="col-md-1"></div>
              <div class="col-md-8">
              <div class="form-group">
            
                  <label for="labelUserName" >User Name</label>
                  
                    <input class="form-control" name="userName" type="text" id="userName" onBlur="checkUserNameAjax()" placeholder="Enter User Name" autocomplete="off" required>
                    <span id="user-availability-status"></span> 
              
                </div>

                <div class="form-group">                  
                  <label for="labelEmail" >Email</label>
                  <input autocomplete="off" required type="email" class="form-control" id="inputEmail" name="email" placeholder="Enter Email">            
                </div>
                
                   <div class="form-group">
                
                  <label for="labelassword" >Password</label>
                  <input autocomplete="off" required type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">
             
                </div>
 


            <div class="form-group">
            
                  <label for="labelFullName" >Client Name</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputFullName" name="fullName" placeholder="Enter Client Name">
           
                </div>
                 
                
                  <div class="form-group">
                  
                  <label for="labelPhone" >Phone</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputPhone" name="phone" placeholder="Enter Phone">
                
                </div>
                <div class="form-group">
                
                  <label for="labelAddress" >Address</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputAddress" name="address" placeholder="Enter Address">
             
                </div>
                  
              
                 <div class="form-group">
                 
                  <label for="labelcompanyRegistrationNumber" >Company Registration Number</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputcompanyRegistrationNumber" name="companyRegistrationNumber" placeholder="Enter Company Registration Number">
              
                </div>
            
                 <div class="form-group">
                 
                  <label for="labelAge" >Since</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputSince" name="since" placeholder="Enter Year">
               
                </div>
                
              
            
                  <div class="box-footer">
                <button type="submit" class="btn btn-primary center-block" name="create_user_submit">Create Client</button>
              </div>
                </div>   <!-- col end -->
              </div>
              <!-- /.box-body -->

            
           <?php echo form_close();?>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
      

        </div>
        <!--/.col (left) -->
   
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
   
    <!-- /.Tables -->
   
     
    
    </section>
    </div>


