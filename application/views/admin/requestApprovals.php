 <div class="content-wrapper">
    <!-- Main content -->
     <section class="content-header">
      <h1>
        Request Approval(booked Car + User)
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Request Approval</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
     
          <!-- /.box -->

          <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <h3 class="box-title">Registered User With Car Data</h3> -->
            </div>
            <!-- /.box-header -->
            <!-- <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet"> -->
           <?php echo form_open('adminController/assignSelectedCar');?>
            <div class="box-body">


<!-- $carResult[0]['remainingCars']; -->

<input type="hidden" value="<?php echo $carResult[0]['remainingCars']; ?>" id="noOfCars" name="">

 <div style="display: none;background: #faebcc !important;color: #8a6d3b !important;border-color:#8a6d3b;" id="showdiv" class="alert alert-warning">
    <strong></strong> The Client has Requested <?php echo $carResult[0]['noOfCars'];?> Car(s),
    <?php if($carResult[0]['noOfAssignedCars'] != 0) { ?>
     <?php echo "You have already assigned ".$carResult[0]['noOfAssignedCars']." Car(s)"; } ?>
  </div>
            <!--   <a  class="btn btn-primary pull-right" href="<?php #echo base_url().'adminController/addCar'; ?>"> Add Car</a> -->
              <table  class="table table-bordered table-striped example1">

                <thead>
                <tr>
                <th></th>
                  <th>Driver Name</th>
                  <th>Father Name</th>
                  <th>Phone</th>
                  <th>Car Model</th>
                  <th>Registration #</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
            <?php foreach ($result as $row) { ?>
           
           
                <tr>
                <td><input type="checkbox" name="selected_row[]" class="checkboxLimit" value="<?php echo $row->carId?>">
                </td>
                  <td> <?php echo $row->fullName?> </td>
                  <td> <?php echo $row->fatherName?> </td>
                  <td> <?php echo $row->phone?> </td>
                  <td> <?php echo $row->carModel?> </td>
                  <td> <?php echo $row->registrationNumber?> </td>

                  <td>
                
<a onclick="view_car_data(<?php echo $row->carId;?>)" class="btn btn-primary">View</a>
          <!-- <a href=<?php #echo base_url().'adminController/#?id='.$row->carId?>><i class="fa fa-eye" aria-hidden="true"></i></a> -->
                                          <!-- view Modal  -->
                     
 <a  href=<?php echo base_url().'adminController/assignSingleCar?carId='.$row->carId.'&request='.$carRequestId?> class="btn btn-success clickme" type="button" class="btn btn-block btn-success">Assign</a>
                  <!-- <a href=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i></i></a> -->
<input type="hidden"  name="carRequestId" value="<?php echo $carRequestId?>">
                   
                  </td>
                  
                </tr>
    <?php  }?>
                
                
               
                </tbody>
              
              </table>
              <button type="submit" class="btn btn-success center-block clickme">Assign Selected</button>
              <?php echo form_close(); ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


  </div>




<!-- //modal in footer -->