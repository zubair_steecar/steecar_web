<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Event
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">View Event</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-xs-12">
      
      
                      <?php if($this->session->flashdata('approveViewEvent_message') != ''){?>
               <div class="box-header with-border" >   
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8"  style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('approveViewEvent_message'); ?>
                  </div>
                  </div>
               <?php }?>
                     <?php if($this->session->flashdata('deleteViewEvent_message') != ''){?>
                      <div class="box-header with-border" >  
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8"  style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('deleteViewEvent_message'); ?>
                  </div>
                 </div> 
               <?php }?>
                          
          <!-- /.box -->
 
          <div class="box">
            <div class="box-header">
             <!--  <h4 class="box-title">View All Events Details</h4> -->
                 <!-- Date and time range -->
             <!--  <div class="form-group">
                <label>Filter By Date:</label>

                <div class="input-group">
                  <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> All History
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                </div>
              </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table class="table table-bordered table-striped example1">
                <thead>
                <tr>
                  <th>Event Name</th>
                  <th>Location</th>
                  <th>Event Date</th>
                  <th>Event Time</th>
                  <th>Created By</th>
                  <th>Status</th>
                  <th style="width: 320px;">Action</th>
                  

                </tr>
                </thead>
                <tbody>
             <?php foreach ($result as $row) { ?> 
                <tr>
                  <td> <?php echo $row->eventName?> </td>
                  <td> <?php echo $row->eventLocation?> </td>
                  <td> <?php echo $row->eventDate?> </td>
                  <td> <?php echo $row->eventTime?> </td>
                  <td> <?php echo $row->userName?> </td>
                  <td> 


                  <?php 
                    if ($row->status == "active") 
                    {
                      echo "<button type='button' class='btn btn-success btn-xs'>".$row->status."</button>"; 
                    }
                    elseif($row->status == "trash" || $row->status == "cancel") 
                    {
                      echo "<button type='button' class='btn btn-danger btn-xs'>".$row->status."</button>"; 
                    }
                    else
                    {
                      echo "<button type='button' class='btn btn-warning btn-xs'>".$row->status."</button>" ;
                    }
                 ?> 

                  </td>
                
                  <td>
                 
                  <a onclick="viewEvent(<?php echo $row->eventId;?>)" class="btn btn-primary">View</a>
                  <a href=<?php echo base_url('adminController/updateViewEvent?id=').$row->eventId;?>    class="btn btn-success">Update</a>
                  <a href=<?php echo base_url('adminController/deleteViewEvent?event=').$row->eventId;?>   class="btn btn-danger">Delete</a>
                  <!-- <a href=<?php #echo base_url('adminController/approveViewEvent?event=').$row->eventId;?>   class="btn btn-warning">Approve</a> --> <!--  hide approve functionality -->
                  <a href=<?php echo base_url('adminController/viewOnMap?event=').$row->eventId;?>   class="btn btn-warning">View on Map</a>
              </td>
                  
                </tr>
                
                  
           <?php } ?>
                
               </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      
      <!-- /.row -->
    </section>


  </div>

      









