 <div class="content-wrapper">
    <!-- Main content -->
     <section class="content-header">
      <h1>
        Update Request Approval
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Request Approval</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
     
          <!-- /.box -->

          <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <h3 class="box-title">Registered User With Car Data</h3> -->
            </div>
            <!-- /.box-header -->
            <!-- <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet"> -->
           <?php echo form_open('adminController/assignSelectedCar');?>
            <div class="box-body">


<!-- $carResult[0]['remainingCars']; -->


           <a href="<?php echo base_url('adminController/requestApprovals?id='.$carRequestId);?>" class="btn btn-success pull-right">Add Cars</a>

              <table  class="table table-bordered table-striped example1">

                <thead>
                <tr>
               
                  <th>Driver Name</th>
                  <th>Father Name</th>
                  <th>Phone</th>
                  <th>Car Model</th>
                  <th>Registration #</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
            <?php foreach ($result as $row) { ?>
          
                <tr>
                
                  <td> <?php echo $row->fullName?> </td>
                  <td> <?php echo $row->fatherName?> </td>
                  <td> <?php echo $row->phone?> </td>
                  <td> <?php echo $row->carModel?> </td>
                  <td> <?php echo $row->registrationNumber?> </td>

                  <td>
                
<a onclick="view_car_data(<?php echo $row->carId;?>)" class="btn btn-primary">View</a>
<a href="<?php echo base_url('adminController/deleteUpdateApprovals?id=').$row->carId;?>" class="btn btn-danger"  type="button">Delete</a>
          <!-- <a href=<?php #echo base_url().'adminController/#?id='.$row->carId?>><i class="fa fa-eye" aria-hidden="true"></i></a> -->
                                          <!-- view Modal  -->
                     

                   
                  </td>
                  
                </tr>
    <?php  }?>
                
                
               
                </tbody>
              
              </table>
              
              <?php echo form_close(); ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


  </div>


