<link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>css/mapStyle.css">    

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Working Location
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Add Working Location</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
             <?php if($this->session->flashdata('add_message') != ''){?>
               <div class="box-header with-border" >   
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8"  style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('add_message'); ?>
                  </div>
                  </div>
               <?php }?>

            <!-- /.box-header -->
            <!-- form start -->
           <?php echo form_open('adminController/addWorkingLocation');?>
              <div class="box-body">
              <div class="col-md-1"></div>

              <div class="col-md-8">



     <div id="pac-container">
      <div class="form-group">
      <label for="labelNoOfCar" >Event Location</label>
        <input name="locationText" class="form-control" id="pac-input" type="text"
            placeholder="Enter a location">
            </div>
             </div>

    <div id="map" style="height: 300px;margin: 0px;padding: 0px;"></div>
    <div id="infowindow-content">
      <img src="" width="16" height="16" id="place-icon">
      <span id="place-name"  class="title"></span><br>
      <span id="place-address"></span>
    </div>






            <!--     <div class="form-group">
                  <label for="exampleInputEmail1" >Area</label>
                  <input autocomplete="off" required type="text" class="form-control" id="exampleInputEmail1" name="locationText" placeholder="Enter Area">
                </div> -->

                <div class="form-group">
                  <label for="exampleInputEmail1" >Latitude</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputLatitude" name="latitude" placeholder="Enter Latitude">
               </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" >Longitude</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputLongitude" name="longitude" placeholder="Enter Longitude">
               </div>
              
           
                <div class="box-footer">
                <button type="submit" class="btn btn-primary center-block" name="add_arae_submit">Add Area</button>
              </div>
              </div>  <!-- col-md-6 -->
              </div><!-- /.box-body -->

              
            <?php echo form_close();?>
            </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
      

        </div>
        <!--/.col (left) -->
   
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
      </section>
      </div>
   

   <!-- AutoCompletMap -->
<script src="<?php echo base_url().'assets/'; ?>js/autoCompleteMap.js"></script>

