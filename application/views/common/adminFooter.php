
  <footer class="main-footer">
    <strong>Copyright &copy;  <a href="<?php echo base_url().'assets/'; ?>https://adminlte.io">SteeCar</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url().'assets/'; ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url().'assets/'; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url().'assets/'; ?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url().'assets/'; ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url().'assets/'; ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url().'assets/'; ?>dist/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url().'assets/'; ?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url().'assets/'; ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->

<script src="<?php echo base_url().'assets/'; ?>bower_components/chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url().'assets/'; ?>dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url().'assets/'; ?>dist/js/demo.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url().'assets/'; ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url().'assets/'; ?>plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url().'assets/'; ?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url().'assets/'; ?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url().'assets/'; ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url().'assets/'; ?>plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
  <script src="<?php echo base_url().'assets/'; ?>plugins/timepicker/bootstrap-timepicker.min.js"></script>


<!-- <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>  -->    <!-- for password validation -->
<script type="text/javascript">                      //for modal
$(document).ready(function(){
    $('.hide').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".box").not(targetBox).hide();
        $(targetBox).show();
    });
});

$(document).ready(function(){
    $('#create_user_message').fadeIn('slow');
    $('#create_user_message').delay(3000).fadeOut();
});

 
   




$(document).ready(function(){                          //for pagination
  $(function () {
    $('.example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
});
  $(document).ready(function() {
  $("#password2").keyup(validate);
});


function validate() {                                    //change password 
  var password1 = $("#password1").val();
  var password2 = $("#password2").val();

    
 
    if(password1 == password2) {
       $("#validate-status11").html("<h5 style='color:green;'><span><i class='fa fa-check' aria-hidden='true'></i> Passwords match!</span></h5>");

    }

    else {
        $("#validate-status11").html("<h5 style='color:red;'><span><i class='fa fa-times' aria-hidden='true'></i> Passwords do not match!</span></h5>");  
    }
    
}
 $(function () {                                              //date range ,select2,icheck radio button ,time picker
 //Date range picker
    $('#reservation').daterangepicker()

       //Initialize Select2 Elements
    $('.select2').select2()

     //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })

           //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })

        //Date picker
    $('#datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true
    })

     //Date picker
    $('#Startdatepicker').datepicker({
       format: 'yyyy-mm-dd',
      autoclose: true

    })

     $('#Enddatepicker').datepicker({
       format: 'yyyy-mm-dd',
      autoclose: true

    })

  

  })

  function checkUserNameAjax() {                         //for checking the username is available or not in database
// $("#loaderIcon").show();
$.ajax({
url: "<?php echo site_url('adminController/checkUserNameAjax/');?>",
data:'userName='+$("#userName").val(),
type: "POST",
success:function(data){
$("#user-availability-status").html(data);
// $("#loaderIcon").hide();
},
error:function (){}
});
}

  function checkCurrentPasswordAjax() {                   
$.ajax({
url: "<?php echo site_url('adminController/checkCurrentPasswordAjax/');?>",
data:'password='+$("#password").val(),
type: "POST",
success:function(data){
  

$("#current-password-status").html(data);
// $("#loaderIcon").hide();
},
error:function (){}
});
}




    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )


//limit the car Request Approvals in requestApprovals view

var noOfCars = $('#noOfCars').val();
$('.checkboxLimit').on('change', function() {
   if($('.checkboxLimit:checked').length > noOfCars) {
       this.checked = false;

      
    $('#showdiv').fadeIn('slow');
    $('#showdiv').delay(9000).fadeOut();

   }
});




$(document).ready(function(){
    var allCars = $("#noOfCars").val();
   
    if(allCars == '0')
    {
    
       $('.clickme').attr("disabled", "disabled");
       $('.clickme').attr("href", "#");
        $('#showdiv').fadeIn('slow');
        $('#showdiv').delay(9000).fadeOut();
    }
});
</script>
  <script>


function show() {    
var myKeyVals = { inputNoOfCar : $("#inputNoOfCar").val()}               
$.ajax({
url: "<?php echo site_url('adminController/checkAvailabelCars/');?>",
data:myKeyVals,
type: "POST",
success:function(data){
  if(data == '0')
  {
    $("#car_request_submit").prop('disabled', false);
    $("#current").html('');
    $("#current").hide();
  }else{
    $("#current").html(data);
    $("#current").show();
    $("#car_request_submit").prop('disabled', true);

  }

},
error:function (){}
});
}

   </script>

 </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_MAP_API_KEY; ?>&libraries=places&callback=initMap"
        async defer></script>





       
<script type="text/javascript">
    function view_car_data(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
 
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('adminController/individual_viewCar/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 
            $('[name="carId"]').val(data.carId);
            $('[name="userName"]').val(data.userName);
            $('[name="fullName"]').val(data.fullName);
            $('[name="fatherName"]').val(data.fatherName);
            $('[name="status"]').val(data.status);
            $('[name="email"]').val(data.email);

            $('[name="phone"]').val(data.phone);
            $('[name="address"]').val(data.address);
            $('[name="age"]').val(data.age);
            $('[name="gender"]').val(data.gender);
           

            $('[name="cnic"]').val(data.cnic);
            $('[name="license"]').val(data.license);
            $('[name="createDate"]').val(data.createDate);

            $('[name="carModel"]').val(data.carModel);
            $('[name="registrationNumber"]').val(data.registrationNumber);
            $('[name="manufacturer"]').val(data.manufacturer);
            $('[name="color"]').val(data.color);
            $('[name="preferenceBranding"]').val(data.preferenceBranding);
            $('[name="trackerId"]').val(data.trackerId);
            $('[name="createDateCar"]').val(data.createDate);
 
 
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('View Car'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }


</script>



<!-- //VIEW MODAL -->
    <div class="modal fade" id="view_user_modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title"></h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="book_id"/>
          <div class="form-body">
           <table class="table table-bordered table-striped">
                <thead>
               
                  <!-- <tr><th>Image</th><td> <?php #echo $row-> ?></td></tr> -->
                                   
                
                  <tr> <th>User Name</th><td><input readonly style="border: none;background: none;" name="userName" class="form-control" type="text"> </td></tr>
                  <tr><th>Client Name</th><td><input readonly style="border: none;background: none;" name="fullName" class="form-control" type="text"> </td></tr>
                  <tr><th>Email</th><td><input readonly style="border: none;background: none;" name="email" class="form-control" type="text">  </td></tr>
                  <tr><th>Since</th><td><input readonly style="border: none;background: none;" name="since" class="form-control" type="text">  </td></tr>
                  <tr><th>Phone</th><td><input readonly style="border: none;background: none;" name="phone" class="form-control" type="text"> </td></tr>
                  <tr><th>Address</th><td><input readonly style="border: none;background: none;" name="address" class="form-control" type="text">  </td></tr>
                  <tr><th>Company Registration Number</th><td><input readonly style="border: none;background: none;" name="companyRegistrationNumber" class="form-control" type="text">  </td></tr>
                  <tr><th>Client Created Date</th><td><input readonly style="border: none;background: none;" name="createDate" class="form-control" type="text">  </td></tr>
                              
                </thead>
               
                </table>
 
          </div>
        </form>
          </div>
          <div class="modal-footer">
            
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->



<script type="text/javascript">
    function viewClient(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
 
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('adminController/individualViewUser/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 
            $('[name="carId"]').val(data.carId);
            $('[name="userName"]').val(data.userName);
            $('[name="fullName"]').val(data.fullName);
            // $('[name="fatherName"]').val(data.fatherName);
            // $('[name="status"]').val(data.status);
            $('[name="email"]').val(data.email);
            $('[name="since"').val(data.since);
              $('[name="phone"]').val(data.phone);
            $('[name="address"]').val(data.address);
            // $('[name="age"]').val(data.age);
            // $('[name="gender"]').val(data.gender);
            // $('[name="address"]').val(data.address);

            // $('[name="cnic"]').val(data.cnic);
            // $('[name="passport"]').val(data.passport);
            $('[name="createDate"]').val(data.createDate);
            // $('[name="carModel"]').val(data.carModel);
            
              // $('[name="preferenceBranding"]').val(data.preferenceBranding);
            $('[name="companyRegistrationNumber"]').val(data.companyRegistrationNumber);
            // $('[name="carCompany"]').val(data.carCompany);
            // $('[name="status"]').val(data.status);
            // $('[name="createDate"]').val(data.createDate);
            // $('[name="color"]').val(data.color);
 
 
            $('#view_user_modal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('View Car'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



</script>


<!-- /view_event_modal -->
     <div class="modal fade" id="viewEventModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title"></h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="book_id"/>
          <div class="form-body">
           <table class="table table-bordered table-striped">
                <thead>
                                
                  <tr><th>Name</th><td> <input readonly style="border: none;background: none;" name="eventName" class="form-control" type="text"> </td></tr>
                  <tr><th>Location</th><td><input readonly style="border: none;background: none;" name="eventLocation" class="form-control" type="text"> </td></tr>
                  <tr><th>Latitude</th><td> <input readonly style="border: none;background: none;" name="locLatitude" class="form-control" type="text"> </td></tr>
                  <tr><th>Longitude</th><td> <input readonly style="border: none;background: none;" name="locLongitude" class="form-control" type="text"> </td></tr>
                  <tr><th>Event Date</th><td> <input readonly style="border: none;background: none;" name="eventDate" class="form-control" type="text"> </td></tr>
                  <tr><th>Event Time</th><td> <input readonly style="border: none;background: none;" name="eventTime" class="form-control" type="text"> </td></tr>
                  <tr><th>Created By</th> <td> <input readonly style="border: none;background: none;" name="createdBy" class="form-control" type="text"> </td></tr>
                  <tr><th>Status</th><td> <input readonly style="border: none;background: none;" name="status" class="form-control" type="text"> </td></tr>
                 
                
                </thead>
            
              </table>
      </div>
        </form>
          </div>
          <div class="modal-footer">
            
            <button type="button" class="btn btn-primary" data-dismiss="modal">close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->


<script type="text/javascript">
    function viewEvent(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
 
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('adminController/individualViewEvent/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 
            $('[name="eventName"]').val(data.eventName);
            $('[name="eventLocation"]').val(data.eventLocation);
            $('[name="locLatitude"]').val(data.locLatitude);
            $('[name="locLongitude"]').val(data.locLongitude);
            $('[name="eventDate"]').val(data.eventDate);
            $('[name="eventTime"]').val(data.eventTime);
            $('[name="createdBy"]').val(data.fullName);
            $('[name="status"]').val(data.status);
            

           
            $('#viewEventModal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('View Event'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



</script>









 <!-- requestAPproval  -->
<!-- //VIEW MODAL -->

<!-- //////////////////////////////// -->

<!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title"></h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="book_id"/>
          <div class="form-body">
           <table class="table table-bordered table-striped">
                <thead>
               
                  <!-- <tr><th>Image</th><td> <?php #echo $row-> ?></td></tr> -->
                                   
                
                  <tr> <th>User Name</th><td><input readonly style="border: none;background: none;" name="userName" class="form-control" type="text"> </td></tr>
                  <tr><th>Driver Name</th><td><input readonly style="border: none;background: none;" name="fullName" class="form-control" type="text"> </td></tr>
                  <tr><th>Father Name</th><td><input readonly style="border: none;background: none;" name="fatherName" class="form-control" type="text">  </td></tr>
                  <tr><th>Email</th><td><input readonly style="border: none;background: none;" name="email" class="form-control" type="text">  </td></tr>
                  <tr><th>Age</th><td><input readonly style="border: none;background: none;" name="age" class="form-control" type="text">  </td></tr>
                  <tr><th>Gender</th><td><input readonly style="border: none;background: none;" name="gender" class="form-control" type="text">  </td></tr>
                  <tr><th>Phone</th><td><input readonly style="border: none;background: none;" name="phone" class="form-control" type="text"> </td></tr>
                  <tr><th>Address</th><td><input readonly style="border: none;background: none;" name="address" class="form-control" type="text">  </td></tr>
                  <tr><th>CNIC Number</th><td><input readonly style="border: none;background: none;" name="cnic" class="form-control" type="text">  </td></tr>
                  <tr><th>License</th><td><input readonly style="border: none;background: none;" name="license" class="form-control" type="text">  </td></tr>
                  <tr><th>User Created Date</th><td><input readonly style="border: none;background: none;" name="createDate" class="form-control" type="text">  </td></tr>
               
                  
                  <tr><th>Car Model</th><td><input readonly style="border: none;background: none;" name="carModel" class="form-control" type="text"> </td></tr>
                  <tr><th>Registration Number</th><td><input readonly style="border: none;background: none;" name="registrationNumber" class="form-control" type="text">  </td></tr>
                
                  <tr><th>Preference Branding</th><td><input readonly style="border: none;background: none;" name="preferenceBranding" class="form-control" type="text">  </td></tr>
                  <tr><th>Manufacturer</th><td><input readonly style="border: none;background: none;" name="manufacturer" class="form-control" type="text">  </td></tr>
                  <tr><th>Car Color</th><td><input readonly style="border: none;background: none;" name="color" class="form-control" type="text">  </td></tr>
                  <tr><th>Car Created Date</th><td><input readonly style="border: none;background: none;" name="createDateCar" class="form-control" type="text">  </td></tr>
                  <tr><th>Tracking Id</th><td><input readonly style="border: none;background: none;" name="trackerId" class="form-control" type="text">  </td></tr>
             

               
                </thead>
               
                </table>
 
          </div>
        </form>
          </div>
          <div class="modal-footer">
            
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->






</body>
</html>
       

       