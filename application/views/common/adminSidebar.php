<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url().'assets/'; ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo ucfirst($this->session->userdata('fullNameAdmin'));?></p>
          <a ><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
           <li>
          <a href="<?php echo base_url('adminController/index');?>">
            <i class="fa fa-calendar"></i> <span> Dashboard</span>
           
          </a>
        </li>

        <li class=" treeview ">
          <a href="#">
            <i class="fa fa-user" aria-hidden="true"></i> <span> Client</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           
             <li><a href="<?php echo base_url().'adminController/createUser'; ?>"><i class="fa fa-circle-o"></i> Create Client</a></li>
         <!--    <li ><a href="<?php #echo base_url().'adminController/viewUser'; ?>"><i class="fa fa-circle-o"></i> View User</a></li> -->

            <li ><a href="<?php echo base_url().'adminController/allUser';?>"><i class="fa fa-circle-o"></i> All Client</a></li>
          
          </ul>
        </li>
        
     
         <li class=" treeview ">
          <a href="#">
            <i class="fa fa-car" aria-hidden="true"></i> <span> Driver</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           
             <li><a href="<?php echo base_url().'adminController/addCar'; ?>"><i class="fa fa-circle-o"></i> Add Driver</a></li>
            <!-- <li ><a href="<?php #echo base_url().'adminController/viewCar'; ?>"><i class="fa fa-circle-o"></i> View Car</a></li> -->

            <li ><a href="<?php echo base_url().'adminController/allCar'; ?>"><i class="fa fa-circle-o"></i> All Driver</a></li>
            <li ><a href="<?php echo base_url().'adminController/availableCar'; ?>"><i class="fa fa-circle-o"></i> Available Driver</a></li>
            <li ><a href="<?php echo base_url().'adminController/viewCar'; ?>"><i class="fa fa-circle-o"></i> Assigned Driver</a></li>

          </ul>
        </li>

         <li class=" treeview ">
          <a href="#">
            <i class="fa fa-calendar" aria-hidden="true"></i> <span> Events</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'adminController/createEvent'; ?>"><i class="fa fa-circle-o"></i> Create Event</a></li>
             <li><a href="<?php echo base_url().'adminController/viewEvent'; ?>"><i class="fa fa-circle-o"></i> View Event</a></li> 

            
          </ul>
        </li>

        <li class=" treeview ">
          <a href="#">
            <i class="ion ion-person-add"></i> <span> Request</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="<?php echo base_url().'adminController/carRequest'; ?>"><i class="fa fa-circle-o"></i>  Car Request</a></li>
             <li><a href="<?php echo base_url().'adminController/allRequest'; ?>"><i class="fa fa-circle-o"></i> View User Request</a></li>
            
          </ul>
        </li>

         <li class=" treeview ">
          <a href="#">
            <i class="fa fa-building-o" aria-hidden="true"></i> <span> Working Location</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           
             <li><a href="<?php echo base_url().'adminController/addWorkingLocation'; ?>"><i class="fa fa-circle-o"></i> Add Working Location</a></li>
            <li ><a href="<?php echo base_url().'adminController/viewWorkingLocation'; ?>"><i class="fa fa-circle-o"></i> View Working Location</a></li>
          </ul>
        </li>


          <li>
          <a href="<?php echo base_url('adminController/defineRate');?>">
            <i class="fa fa-calendar"></i> <span> Define Rate</span>
           
          </a>
        </li>

         <li>
          <a href="<?php echo base_url().'adminController/logout';?>">
            <i class="fa fa-sign-out" aria-hidden="true"></i> <span> Sign Out</span>
           </a>
        </li>




               
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>