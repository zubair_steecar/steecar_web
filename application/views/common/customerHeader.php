<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SteeCar</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>bower_components/bootstrap-daterangepicker/daterangepicker.css">

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>plugins/iCheck/all.css">
   <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>bower_components/select2/dist/css/select2.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>bower_components/Ionicons/css/ionicons.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>dist/css/skins/_all-skins.min.css">
 <link rel="stylesheet"  href="<?php echo base_url().'assets/'; ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="<?php echo base_url().'assets/'; ?>https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href=<?php echo base_url('customerController');?> class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>STC</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SteeCar</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="<?php echo base_url().'assets/'; ?>#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
         <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="<?php echo base_url().'assets/'; ?>#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url().'assets/'; ?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('fullName');?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url().'assets/'; ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>

                  <?php echo  ucfirst($this->session->userdata('userType'))." - ".ucfirst($this->session->userdata('fullName'));?>
                  <small><?php echo $this->session->userdata('email');?></small>
                </p>
              </li>             
              <!-- Menu Footer-->
              <li class="user-footer"> 
              <div class="pull-left">
              <a data-toggle="modal" data-target="#view_modal" href="#" class="btn btn-default btn-flat"><i class="fa fa-key" aria-hidden="true"></i>
 Change Password</a> <!-- MOdal end of page -->
             
                            
                </div>              
                <div class="pull-right">
                    <a href="<?php echo base_url().'customerController/signOut';?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out" aria-hidden="true"></i> Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="<?php #echo base_url().'assets/'; ?>#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>


    </nav>
  </header>
 <!-- Change Password Modal  END-->

           <div class="modal fade" id="view_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title"><i class="fa fa-key" aria-hidden="true"></i>
 Change your Password</h4>
        </div>
        <?php echo form_open('customerController/change_password');?>
        <div class="modal-body">

         <div class="form-group">
                 <div class="row">
        <label for="exampleInputEmail1" class="col-md-4 control-label" style="text-align: right;">New Password</label>
        <div class="col-md-6 offset 2">
           <input autocomplete="off" required type="password" class="form-control" id="password1" name="npassword" placeholder="Enter New Password" required>
         </div>
         </div>
         </div> 
          
            <div class="form-group">
                 <div class="row">
          <label for="exampleInputEmail1" class="col-md-4 control-label" style="text-align: right;">Confrim Password</label>
          <div class="col-md-6 offset 2">
          <input autocomplete="off" required type="password" class="form-control" id="password2" name="cpassword" placeholder="Enter Confrim Password"><p id="validate-status11" required></p>


           </div>
           </div>
          </div>

        </div>
        <div class="modal-footer">
          
           <button type="submit" name="change_password_submit" class="btn btn-default center-block"   >Confrim</button>
           <?php echo form_close();?>
        </div>
      </div>
      
    </div>
  </div>