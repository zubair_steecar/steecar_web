<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url().'assets/'; ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo ucfirst($this->session->userdata('fullName'));?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
    
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

         <li>
          <a href="<?php echo base_url().'customerController/index';?>">
            <i class="fa fa-calendar"></i> <span> Dashboard</span>
           </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="ion ion-person-add"></i> <span>Request</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="<?php echo base_url().'customerController/carRequest'; ?>"><i class="fa fa-circle-o"></i> Car Request</a></li>
            <li ><a href="<?php echo base_url().'customerController/allRequest'; ?>"><i class="fa fa-circle-o"></i> All Request</a></li>
          </ul>
        </li>
        

           <li class="treeview ">
          <a href="#">
            <i class="fa fa-car" aria-hidden="true"></i> <span>Cars</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="<?php echo base_url().'customerController/carDetails'; ?>"><i class="fa fa-circle-o"></i> Car Details</a></li>
            <li ><a href="<?php echo base_url().'customerController/onMap'; ?>"><i class="fa fa-circle-o"></i> On Map</a></li>
          </ul>
        </li>

           <li class="treeview ">
          <a href="#">
            <i class="fa fa-calendar" aria-hidden="true"></i> <span>Event</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           <li><a href="<?php echo base_url().'customerController/createEvent'; ?>"><i class="fa fa-circle-o"></i> Create Event</a></li>
            <li ><a href="<?php echo base_url().'customerController/viewEvent'; ?>"><i class="fa fa-circle-o"></i> View Event</a></li>
          </ul>
        </li>

<!-- 
         <li>
          <a href="<?php #echo base_url().'customerController/impression'; ?>">
            <i class="fa fa-calendar"></i> <span> Car Impression</span>
           </a>
        </li>

        <li>
        <a href="<?php #echo base_url().'customerController/editProfile'; ?>">
            <i class="fa fa-calendar"></i> <span> Edit Profile</span>
           </a>
        </li> -->

        <li>
          <a href="<?php echo base_url().'customerController/signOut';?>">
            <i class="fa fa-sign-out" aria-hidden="true"></i> <span> Sign Out</span>
           </a>
        </li>
      
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>