<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Request
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">All Request</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
      
          <!-- /.box -->
 
          <div class="box">
            <div class="box-header">
             <!--  <h4 class="box-title">View All Car Request</h4> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table class="table table-bordered table-striped example1">
                <thead>
                <tr>
                  <th>No. of Car</th>
                  <th>Assigned Cars</th>
                  <th>Remaining Cars</th>
                  <!-- <th>Car Model</th> -->
                  <th>Preference Branding</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Status</th>
                  <!-- <th>approveDate</th> -->
                  <!-- <th>approveBy</th> -->
                  <th>Action</th>

                </tr>
                </thead>
                <tbody>
                 <?php foreach ($result as $row) { ?>

                 <?php $noOfCars = $row->noOfCars;
                  $noOfAssignedCars = $row->noOfAssignedCars;
                  $remainingCars = $row->noOfCars - $row->noOfAssignedCars; ?>
                <tr>
                  <td> <?php echo  $row->noOfCars;echo($row->noOfCars == $row->noOfAssignedCars)? "&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: green;' class='glyphicon glyphicon-ok-sign'></span>" : '' ?> </td>

                  <?php if($row->noOfAssignedCars != 0 ) {?>
                  <td><b><a href="<?php echo base_url('customerController/assignedCarDetails?id=').$row->carRequestId;?>"><?php echo $row->noOfAssignedCars;?> <i class="fa fa-external-link"></i></a></b></td>
                  <?php }else{?>
                  <td><?php echo $row->noOfAssignedCars;?></td>
                  <?php }?>

                  <td><?php echo $remainingCars; ?></td>
                  <!-- <td> <?php echo $row->carModel?> </td> -->
                  <td> <?php echo $row->preferenceBranding?> </td>
                  <td> <?php echo $row->startDate?> </td>
                  <td> <?php echo $row->endDate?> </td>
                  <td> <?php 

                     if ($row->status == "approved") 
                      {
                          echo "<button type='button' class='btn btn-success btn-xs'>".$row->status."</button>"; 
                      }
                      elseif($row->status == "cancel") 
                      {
                          echo "<button type='button' class='btn btn-danger btn-xs'>".$row->status."</button>"; 
                      }
                      else
                      {
                          echo "<button type='button' class='btn btn-warning btn-xs'>".$row->status."</button>" ;
                      }

                  ?> </td>

                 <!--  <td><?php echo $row->approveDate ?></td>
                  <td>
                  <?php 
                    if ($row->approveBy == "Pending Request") 
                  {
                    echo "<button type='button' class='btn btn-warning btn-xs'>".$row->approveBy."</button>";
                  }
                  else
                  {
                    echo "<button type='button' class='btn btn-success btn-xs'>".$row->approveBy."</button>";
                  }
                  ?>
                  </td> -->




                <td>
                 
                <a href=<?php echo base_url('customerController/updateAllRequest?id=').$row->carRequestId;?>  class="btn btn-success">Update</a>
                <a href=<?php echo base_url('customerController/deleteAllRequest?id=').$row->carRequestId;; ?> class="btn btn-danger">Delete</a>
               </td>
                    
                  
                </tr>
                  
            <?php } ?>
                
               </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


  </div>

      

