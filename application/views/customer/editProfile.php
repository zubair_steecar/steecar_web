<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Profile         
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Edit Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <!-- <div class="col-md-3"></div> -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
             
                     <!-- Inserted Area Successfully  -->
                <?php if($this->session->flashdata('edit_profile_message') != ''){?>
                <br>
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-7">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('edit_profile_message'); ?>
                  </div>
               <?php }?> 
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('SiteController/createUser');?>
            
            <input type="hidden" value="customer" name="userType">
              <div class="box-body">
              <div class="col-md-1"></div>
              <div class="col-md-8">
              <div class="form-group">
            
                  <label for="labelUserName" >User Name</label>
                  
                  <input autocomplete="off" required type="text" class="form-control" id="inputUserName" name="userName" placeholder="Enter User Name">
              
                </div>
            <div class="form-group">
            
                  <label for="labelFullName" >Full Name</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputFullName" name="fullName" placeholder="Enter Full Name">
           
                </div>
                  <div class="form-group">
                  
                  <label for="labelFatherName" >Father Name</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputFatherName" name="fatherName" placeholder="Enter Father Name">
            
                </div>
                 <div class="form-group">
                 
                 <label for="label" > Select Gender</label><br>
                  <input type="radio" value="male" name="gender" > Male
                   <input type="radio" value="female" name="gender" > Female
             
                </div>
                  <div class="form-group">
                  
                  <label for="labelPhone" >Phone</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputPhone" name="phone" placeholder="Enter Phone">
                
                </div>
                <div class="form-group">
                
                  <label for="labelAddress" >Address</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputAddress" name="address" placeholder="Enter Address">
             
                </div>
                  <div class="form-group">
                  
                  <label for="labelEmail" >Email</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputEmail" name="email" placeholder="Enter Email">
              
                </div>
              
                 <div class="form-group">
                 
                  <label for="labelCnic" >CNIC Number</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputCnic" name="cnic" placeholder="Enter CNIC Number">
              
                </div>
                <div class="form-group">
                
                  <label for="labelPassport" >Passport Number</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputPassport" name="passport" placeholder="Enter CNIC Number">
                
                </div>
                 <div class="form-group">
                 
                  <label for="labelAge" >Age</label>
                  <input min="0" autocomplete="off" required type="number" class="form-control" id="inputAge" name="age" placeholder="Enter Age">
               
                </div>
                
                <div class="form-group">
                
                  <label for="labelassword" >Password</label>
                  <input autocomplete="off" required type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">
             
                </div>
                <div class="form-group">
                
                  <label for="labelFileName" >Image</label>
                  <input autocomplete="off" required type="file" id="inputFile" name="file_name">
                  
                </div>
                  <div class="box-footer">
                <button type="submit" class="btn btn-primary center-block" name="create_user_submit">Create User</button>
              </div>
                </div>   <!-- col end -->
              </div>
              <!-- /.box-body -->

            
           <?php echo form_close();?>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
      

        </div>
        <!--/.col (left) -->
   
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
   
    <!-- /.Tables -->
   
     
    
    </section>
    </div>


