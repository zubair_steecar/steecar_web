<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Car Details
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Car Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
      
          <!-- /.box -->
 
          <div class="box">
            <div class="box-header">
              <!-- <h4 class="box-title">View All Car Request</h4> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table class="table table-bordered table-striped example1">
                <thead>
                <tr>
                  
                  <th>Driver Name</th>
                  <th>Phone</th>  
                  <th>Registration Number</th>
                  <th>Car Model</th>
                                  
                  <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <?php foreach ($result as $row) { ?>
                <tr>
                  <td> <?php echo $row->fullName?> </td>
                  <td> <?php echo $row->phone?> </td>
                  <td> <?php echo $row->registrationNumber?> </td>
                  <td> <?php echo $row->carModel?> </td>
                  
                  <td>
                 
                  <a onclick="view_car_data(<?php echo $row->carId;?>)"  class="btn  btn-primary">View</a>
               
  <a href="<?php echo base_url('customerController/deleteUpdateApprovals?id=').$row->carId;?>"  class="btn  btn-danger">Remove</a>
                 
  </td>
                    
                  
                </tr>
                  
            <?php } ?>
                
               </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>