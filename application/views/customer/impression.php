<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Car Impression
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Car Impression</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
      
          <!-- /.box -->
 
          <div class="box">
            <div class="box-header">
              <!-- <h4 class="box-title">View All Car Request</h4> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table class="table table-bordered table-striped example1">
                <thead>
                <tr>
                  <th>Car Number</th>
                  <th>Driver Name</th>
                  <th>Car Distance</th>
                  <th>Total Impression</th>
                  
                  <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <!-- <?php #foreach ($result as $row) { ?> end of for loop--> 
                <tr>
                  <td>007<!-- <?php #echo $row->numberOfCar?> --></td>
                  <td>ALi<!-- <?php #echo $row->carModal?> --></td>
                  <td>20 Km<!-- <?php #echo $row->location?> --></td>
                  <td>888<!-- <?php #echo $row->dateRange?> --></td>
                  <td>
                 
                  <a href="" data-toggle="modal" data-target="#impressionView_modal"  class="btn  btn-primary">View</a>
               
  <a href="" data-toggle="modal" data-target="#my_update_modal" class="btn  btn-danger">Delete</a>
                 
  </td>
                    
                  
                </tr>
                  
            <!-- end of forloop -->
                
               </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


  </div>

     





<!-- // Impression View modal -->
   <div class="modal fade" id="impressionView_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Car Impression</h4>
        </div>
        <div class="modal-body">
              <table class="table table-bordered table-striped">
                <thead>
                  <tr><th>Car Number</th><td>007</td></tr>
                  <tr><th>Driver Name</th> <td>ALi</td></tr>
                  <tr><th>Car Distance</th> <td>20 KM</td></tr>
                  <tr><th>Total Impression</th> <td>88</td></tr>
                 </thead>
            
              </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>