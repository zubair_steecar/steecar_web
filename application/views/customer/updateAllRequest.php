<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Car Request
      </h1>
     
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Car Request</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            
              <!-- <h3 class="box-title">Request for Car</h3> -->
              <!-- Inserted Area Successfully  -->
             
            <?php if($this->session->flashdata('updateAllRequest_message') != ''){?>
                 <div class="box-header with-border" > 
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8"  style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('updateAllRequest_message'); ?>
                  </div>
                    </div> 
               <?php }?>
             
             
            <!-- /.box-header -->
            <!-- form start -->
           <?php echo form_open('customerController/updateAllRequest');?>

          <div class="box-body">
              <div class="col-md-1"></div>

              <div class="col-md-8">
   <?php foreach ($result as $row) { 
                
              
 }?>
               <input type="hidden" name="carRequestId" value="<?php echo  $row->carRequestId?>">
              <div class="form-group">
                  <label for="labelNoOfCar" >No. of Car</label>
                  <input value="<?php echo $row->noOfCars;?>" autocomplete="off" min="0" required type="number" class="form-control" id="inputNoOfCar" name="NoOfCar" placeholder="Enter No. of Car">
              </div>


          
                   <div class="form-group">
                  <label for="labelPrefrenceBranding" >Preference Branding</label> <br>
                  <input <?php echo ($row->preferenceBranding == 'full') ? 'checked':'';?> type="radio" value="full" name="preferenceBranding" id="exampleInputprefrenceBranding"> Full
                  <input <?php echo ($row->preferenceBranding == 'partial') ? 'checked':'';?> type="radio" value="partial" name="preferenceBranding" > Partial
              </div>


       
        <!-- Date range -->
          <div class="form-group">
                <label>Start Date:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input value="<?php echo $row->startDate ;?>" type="text" class="form-control pull-right" name="startDate" id="Startdatepicker" autocomplete="off" required>
                </div>
                <!-- /.input group -->
          </div>

           <div class="form-group">
                <label>End Date:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input value="<?php echo $row->endDate;?>" type="text" class="form-control pull-right" name="endDate" id="Enddatepicker" autocomplete="off" required>
                </div>
                <!-- /.input group -->
          </div>


              <div class="box-footer">
                <button type="submit" class="btn btn-primary center-block" name="updateAllRequest_submit">
                  Car Request
                </button>
              </div>
          </div>  <!-- col-md-6 -->
      </div><!-- /.box-body -->

             
            <?php echo form_close();?>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
      

        </div>
        <!--/.col (left) -->
   
        
        <!--/.col (right) -->
      </div>

      </section>
      </div>   <!-- content-wrapper -->