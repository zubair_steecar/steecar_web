<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Car Request
      </h1>
      <?php if($this->session->flashdata('not_match_pass_message') != ''){?>
                <br>
                   <div class="alert alert-danger alert-dismissable col-md-4">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('not_match_pass_message'); ?>
                  </div>
               <?php }?>
               <?php if($this->session->flashdata('password_message') != ''){?>
                <br>
                   <div class="alert alert-success alert-dismissable col-md-4">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('password_message'); ?>
                  </div>
               <?php }?>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Car Request</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <h3 class="box-title">Request for Car</h3> -->
              <!-- Inserted Area Successfully  -->
                <?php if($this->session->flashdata('add_message') != ''){?>
                <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('add_message'); ?>
                  </div>
               <?php }?>
            </div>
             <div class="box-header with-border" >
                  <?php if($this->session->flashdata('carRequest_message') != ''){?>
                  
                 <div class="col-md-1"></div>
                   <div class="alert alert-success alert-dismissable col-md-8"  style="background: #bbecd6 !important; color: green !important; display: none;" id="create_user_message">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                   <strong><i class="fa fa-check" aria-hidden="true"></i></strong><?php echo $this->session->flashdata('carRequest_message'); ?>
                  </div>
                  
               <?php }?>
               </div>
            <!-- /.box-header -->
            <!-- form start -->
           <?php echo form_open('customerController/carRequest');?>
          <div class="box-body">
              <div class="col-md-1"></div>

              <div class="col-md-8">

              <div class="form-group">
                  <label for="labelNoOfCar" >No. of Car</label>
                  <input autocomplete="off" min="1" required type="number" onBlur="show();" class="form-control" id="inputNoOfCar" name="NoOfCar" placeholder="Enter No. of Car">
                  <span id="current"></span>
              </div>

            <!--   <div class="form-group">
                  <label for="labelCarModel" >Car Model</label>
                  <input autocomplete="off" required type="text" class="form-control" id="inputCarModel" name="CarModel" placeholder="Enter Car Model">
              </div> -->

          
                   <div class="form-group">
                  <label for="labelPrefrenceBranding" >Preference Branding</label> <br>
                  <input type="radio" value="full" name="preferenceBranding" id="exampleInputprefrenceBranding"> Full
                  <input type="radio" value="partial" name="preferenceBranding" > Partial
              </div>


             <!-- SELECT2 EXAMPLE -->
          <!--   <div class="form-group">
                   <label>Location</label>
                   <select class="form-control select2" multiple="multiple" data-placeholder="Select a Location"
                    style="width: 100%;">
                  <option>Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
            </div> -->
              <!-- /.form-group -->
        <!-- Date range -->
          <div class="form-group">
                <label>Start Date:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="startDate" id="Startdatepicker" autocomplete="off" required>
                </div>
                <!-- /.input group -->
          </div>

           <div class="form-group">
                <label>End Date:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="endDate" id="Enddatepicker" autocomplete="off" required>
                </div>
                <!-- /.input group -->
          </div>


              <div class="box-footer">
                <button type="submit" class="btn btn-primary center-block" id="car_request_submit" name="car_request_submit">
                  Car Request
                </button>
              </div>
          </div>  <!-- col-md-6 -->
      </div><!-- /.box-body -->

             
            <?php echo form_close();?>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
      

        </div>
        <!--/.col (left) -->
   
        
        <!--/.col (right) -->
      </div>

      </section>
      </div>   <!-- content-wrapper -->