<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Event
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">View Event</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-xs-12">
      
          <!-- /.box -->
 
          <div class="box">
            <div class="box-header">
             <!--  <h4 class="box-title">View All Events Details</h4> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table class="table table-bordered table-striped example1">
                <thead>
                <tr>
                  <th>Event Name</th>
                  <th>Location</th>
                  <th>Latitude</th>
                  <th>Longitude</th>
                  <th>Event Date</th>
                  <th>Event Time</th>
                  
                  <!-- <th>Created By</th> -->
                  <th>Status</th>
                  <th style="width: 320px;">Action</th>
                  

                </tr>
                </thead>
                <tbody>
             <?php foreach ($result as $row) { ?> 
                <tr>
                  <td> <?php echo $row->eventName?> </td>
                  <td> <?php echo $row->eventLocation?> </td>
                  <td> <?php echo $row->locLatitude?> </td>
                  <td> <?php echo $row->locLongitude?> </td>
                  <td> <?php echo $row->eventDate?> </td>
                  <td> <?php echo $row->eventTime?> </td>
               
                 <!-- <td> <?php #echo $row->userId?> </td> -->
                  <td> 

                   <?php 
                     

                  
                      if ($row->status == "active") 
                      {
                          echo "<button type='button' class='btn btn-success btn-xs'>".$row->status."</button>"; 
                      }
                      elseif($row->status == "trash" || $row->status == "cancel") 
                      {
                          echo "<button type='button' class='btn btn-danger btn-xs'>".$row->status."</button>"; 
                      }
                      else
                      {
                      
                          echo "<button type='button' class='btn btn-warning btn-xs'>".$row->status."</button>" ;
                      }
                   
                 ?> 

                  </td>
                   <td>
                 
                  <a onclick="viewEvent(<?php echo $row->eventId;?>)" class="btn btn-primary">View</a>
                  <a href=<?php echo base_url('customerController/updateViewEvent?id=').$row->eventId;?>    class="btn btn-success">Update</a>
                  <a href=<?php echo base_url('customerController/deleteViewEvent?event=').$row->eventId;?>   class="btn btn-danger">Delete</a>
                   <a href=<?php echo base_url('customerController/viewOnMap?event=').$row->eventId;?>   class="btn btn-warning">View on Map</a>
             
                  
                </tr>
                </td>
                  
           <?php } ?>
                
               </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      
      <!-- /.row -->
    </section>


  </div>

      








<!-- /view_event_modal -->
     <div class="modal fade" id="viewEventModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title"></h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="book_id"/>
          <div class="form-body">
           <table class="table table-bordered table-striped">
                <thead>
                                
                  <tr><th>Name</th><td> <input readonly style="border: none;background: none;" name="eventName" class="form-control" type="text"> </td></tr>
                  <tr><th>Location</th><td><input readonly style="border: none;background: none;" name="eventLocation" class="form-control" type="text"> </td></tr>
                  <tr><th>Latitude</th><td> <input readonly style="border: none;background: none;" name="locLatitude" class="form-control" type="text"> </td></tr>
                  <tr><th>Longitude</th><td> <input readonly style="border: none;background: none;" name="locLongitude" class="form-control" type="text"> </td></tr>
             
                  <tr><th>Start Time</th><td> <input readonly style="border: none;background: none;" name="eventTime" class="form-control" type="text"> </td></tr>
                  <tr><th>End Date</th> <td> <input readonly style="border: none;background: none;" name="eventDate" class="form-control" type="text"> </td></tr>
                  
                  
                  <tr><th>Status</th><td> <input readonly style="border: none;background: none;" name="status" class="form-control" type="text"> </td></tr>
                  
                
                </thead>
            
              </table>
      </div>
        </form>
          </div>
          <div class="modal-footer">
            
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->


<script type="text/javascript">
    function viewEvent(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
 
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('customerController/individualViewEvent/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 
            $('[name="eventName"]').val(data.eventName);
            $('[name="eventLocation"]').val(data.eventLocation);
            $('[name="locLatitude"]').val(data.locLatitude);
            $('[name="locLongitude"]').val(data.locLongitude);
   
            $('[name="eventTime"]').val(data.eventTime);

              $('[name="eventDate"]').val(data.eventDate);
           
           
            $('[name="status"]').val(data.status);
     

           
            $('#viewEventModal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('View Event'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }




// $(document).ready(function(id){                        //close the label after 3 second

//     $.ajax({
//         url: "<?php #echo site_url('adminController/update/')?>/" + id,
//         data:{ trash:"trash"},
//         type: "POST",
//         success: function(data)
//         {
//           $("#trash_status").html(data);
//         },


//         error: function (jqXHR, textStatus, errorThrown)
//         {
//             alert('Error get data from ajax');
//         }

// });



</script>


