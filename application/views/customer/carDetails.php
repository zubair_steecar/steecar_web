<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Car Details
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">Car Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <div class="row">
        <div class="col-xs-12">
      
          <!-- /.box -->
 
          <div class="box">
            <div class="box-header">
             <!--  <h4 class="box-title">View All Car Request</h4> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table class="table table-bordered table-striped example1">
                <thead>
                <tr>
                 
                  <th>Car ID</th>                      <!-- //syestem generate id -->
                  <th>Preference Branding</th>
                  <th>Total Distance</th>
                  <th>Impression</th>
                  <!-- <th>Status</th> -->
                  <th>Action</th>

                </tr>
                </thead>
                <tbody>
                 <?php foreach ($result as $row) { 
                  
                  $carId = date("ym", strtotime($row->createDate)); 
                  $generatedCarId = $carId."-".$row->carId;
                  ?> 
                <tr>
                  
                  <td> <?php echo $generatedCarId; ?> </td>
                  <td><?php echo $row->preferenceBranding?> </td>
                  <td>200 km <!-- <?php echo $row->totalDistance?> --> </td>
                  <td>300</td>
                   <!-- <td>Active <?php echo $row->status?> </td>  -->
                  
                  <td>
                 
                  <!-- <a href="" data-toggle="modal" data-target="#view_carDetail_modal"   class="btn  btn-primary"> -->
                  <!-- View Detail</a> -->
                  <a href="" data-toggle="modal" data-target="#OnMapView_modal"   class="btn  btn-success">View on Map</a>
                   <a href="" data-toggle="modal" data-target="#OnMapView_modal"   class="btn  btn-warning">Live on Map</a>
                  
  </td>
                   
                  
                </tr>
                  
         <?php } ?>
                
               </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


  </div>
     


<?php
if(1 == 2) { ?>
<!-- view_carDetail_modal -->

 <div class="modal fade" id="view_carDetail_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Car Detail</h4>
        </div>
        <div class="modal-body">
        <table class="table table-bordered table-striped">
          <thead>
                  <tr><th>Car Name</th><td> Civic </td></tr>
                  <tr><th>Driver Name</th><td> Ali</td></tr>
                  <tr><th>Registration Number</th><td> 007</td></tr>
                  <tr><th>Preference Branding</th><td> xxx</td></tr>
                  <tr><th>Total Distance</th><td> 20 KM</td></tr>
                  <tr><th>Status</th><td>Active</td></tr>
          </thead>
        </table>
                  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <?php } ?>

<!-- OnMapView_modal -->
  <div class="modal fade" id="OnMapView_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">View on Map</h4>
        </div>
        <div class="modal-body">
         
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>css/mapStyle.css">
    <div id="map" style="height: 300px;margin: 0px;padding: 0px;"></div>
    <div id="infowindow-content">
      <img src="" width="16" height="16" id="place-icon">
      <span id="place-name"  class="title"></span><br>
      <span id="place-address"></span>
    </div>

<!-- AutoCompletMap -->
<script src="<?php echo base_url().'assets/'; ?>js/autoCompleteMap.js"></script>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

