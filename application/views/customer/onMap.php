<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          Cars on Map
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">on Map</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-body">
              <div class="col-md-1"></div>

                  <div class="col-md-10">
                    <div class="box-header with-border">
                      
                         <?php echo $map['js']; ?>
                         <?php echo $map['html']; ?>
                    </div><!-- /.box-header -->
                  </div>  <!-- col-md-6 -->
            </div><!-- /.box-body -->

              
           
          </div><!-- /.box -->
        </div><!--/.col (left) -->
      </div>

    </section>
</div>   <!-- content-wrapper -->