 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $totalCarsRequest?></h3>

              <p>Requests</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo base_url('customerController/allRequest');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $totalEvents?></h3>

              <p>Events</p>
            </div>
            <div class="icon">
              <i class="fa fa-calendar" aria-hidden="true"></i>
            </div>
            <a href="<?php echo base_url('customerController/viewEvent');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $totalAssignedCar?></h3>

              <p>Cars</p>
            </div>
            <div class="icon">
              <i class="fa fa-car" aria-hidden="true"></i>
            </div>
            <a href="<?php echo base_url('customerController/carDetails');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>

   
     

         <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-xs-12">
      
          <!-- /.box -->
 
          <div class="box">
            <div class="box-header">
              <h4 class="box-title">Events</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table  class="table table-bordered table-striped example1">
                <thead>
                <tr>
                  <th>Event Name</th>
                  <th>Location</th>
                  <th>Latitude</th>
                  <th>Longitude</th>
                  <th>Event Date</th>
                  <th>Event Time</th>
                  
                  <!-- <th>Created By</th> -->
                  <th>Status</th>
                  <th style="width: 320px !important ;">Action</th>
                  

                </tr>
                </thead>
                <tbody>
             <?php foreach ($result as $row) { ?> 
                <tr>
                  <td> <?php echo $row->eventName?> </td>
                  <td> <?php echo $row->eventLocation?> </td>
                  <td> <?php echo $row->locLatitude?> </td>
                  <td> <?php echo $row->locLongitude?> </td>
                  <td> <?php echo $row->eventDate?> </td>
                  <td> <?php echo $row->eventTime?> </td>
               
                 <!-- <td> <?php #echo $row->userId?> </td> -->
                  <td> 

                   <?php 
                     

                  
                      if ($row->status == "active") 
                      {
                          echo "<button type='button' class='btn btn-success btn-xs'>".$row->status."</button>"; 
                      }
                      elseif($row->status == "trash" || $row->status == "cancel") 
                      {
                          echo "<button type='button' class='btn btn-danger btn-xs'>".$row->status."</button>"; 
                      }
                      else
                      {
                      
                          echo "<button type='button' class='btn btn-warning btn-xs'>".$row->status."</button>" ;
                      }
                   
                 ?> 

                  </td>
                   <td>
                 
                  <a onclick="viewEvent(<?php echo $row->eventId;?>)" class="btn btn-primary ">View</a>
                  <a href=<?php echo base_url('customerController/updateViewEvent?id=').$row->eventId;?>    class="btn btn-success ">Update</a>
                  <a href=<?php echo base_url('customerController/deleteViewEvent?event=').$row->eventId;?>   class="btn btn-danger ">Delete</a>
                   <a href=<?php echo base_url('customerController/viewOnMap?event=').$row->eventId;?>   class="btn btn-warning ">View on Map</a>
             
                  
                </tr>
                </td>
                  
           <?php } ?>
                
               </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      
      <!-- /.row -->
    </section>

<!-- //car details page  -->
      <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-xs-12">
      
          <!-- /.box -->
 
          <div class="box">
            <div class="box-header">
              <h4 class="box-title">Car Details</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<table  class="table table-bordered table-striped example1">
                <thead>
                <tr>
                 
                  <th>Car ID</th>                      <!-- //syestem generate id -->
                  <th>Preference Branding</th>
                  <th>Total Distance</th>
                  <th>Impression</th>
                  <!-- <th>Status</th> -->
                  <th>Action</th>

                </tr>
                </thead>
                <tbody>
                 <?php foreach ($resultCarDetails as $row) { 
                  
                  $carId = date("ym", strtotime($row->createDate)); 
                  $generatedCarId = $carId."-".$row->carId;
                  ?> 
                <tr>
                  
                  <td> <?php echo $generatedCarId; ?> </td>
                  <td><?php echo $row->preferenceBranding?> </td>
                  <td>200 km <!-- <?php echo $row->totalDistance?> --> </td>
                  <td>300</td>
                   <!-- <td>Active <?php echo $row->status?> </td>  -->
                  
                  <td>
                 
                  <!-- <a href="" data-toggle="modal" data-target="#view_carDetail_modal"   class="btn  btn-primary"> -->
                  <!-- View Detail</a> -->
                  <a href="" data-toggle="modal" data-target="#OnMapView_modal"   class="btn  btn-success">View on Map</a>
                   <a href="" data-toggle="modal" data-target="#OnMapView_modal"   class="btn  btn-warning">Live on Map</a>
                  
  </td>
                   
                  
                </tr>
                  
         <?php } ?>
                
               </tbody>
              </table>

                </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      
      <!-- /.row -->
    </section>
<!-- end car detail page -->


</section>
 </div>