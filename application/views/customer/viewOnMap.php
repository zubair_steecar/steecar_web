<style>
  #map {
  height: 400px;
  width: 100%;
 }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          View on Map
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Forms</a></li> -->
        <li class="active">View on  Map</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-body">
              <div class="col-md-1"></div>

                  <div class="col-md-10">
                    <div class="box-header with-border">
                <?php foreach ($result as $row) {?> 
                <div class="col-md-10">
                  <table  class="table table-bordered table-striped">
         
                <tr>
                  <td style="width: 50%;"><i class="fa fa-address-card margin-r-5"></i><?php echo $row->eventName?></td>
                   <td><i class="fa fa-calendar margin-r-5"></i><?php echo $row->eventDate?></td>
                </tr>
               
                <tr>
                  <td><i class="fa fa-map-marker margin-r-5"></i> <?php echo $row->eventLocation?></td>
                  <td><i class="fa fa-clock-o margin-r-5"></i> <?php echo $row->eventTime?></td>
                </tr> 
         
                </table>
                  </div>
                
                      <input  id="latitude" type="hidden" name="latitude" value="<?php echo $row->locLatitude?>">
                      <input  id="longitude" type="hidden" name="longitude" value="<?php echo $row->locLongitude?>">
                      <?php }?> 
                     <div id="map"></div>


                    </div><!-- /.box-header -->
                  </div>  <!-- col-md-6 -->
            </div><!-- /.box-body -->

              
           
          </div><!-- /.box -->
        </div><!--/.col (left) -->
      </div>

    </section>
</div>   <!-- content-wrapper -->



<script>
  
  function initMap() {

  var latitude = $('#latitude').val();
  var longitude = $('#longitude').val();

  var uluru = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
  //alert(uluru);
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 18,
    center: uluru
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });
}

</script>