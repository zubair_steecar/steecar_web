<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminController extends CI_Controller {
	 function __construct() 
	 {
        parent::__construct();
        if (($this->session->userdata('userTypeAdmin') != 'admin') && ($this->session->userdata('userIdAdmin') == null)) {
        	redirect('loginController/login');

        }

       
        $this->load->model('adminModel');
    }

	public function index()
	{
		$answer['totalEvents'] = $this->adminModel->totalEvents();
		$answer['totalUser'] = $this->adminModel->totalUser();
		$answer['totalRquests'] = $this->adminModel->totalRquests();
		// $answer['totalWorkingLocation'] = $this->adminModel->totalWorkingLocation();
		$answer['totalDrivers'] = $this->adminModel->totalDrivers();
		$answer['result'] = $this->adminModel->viewEvent();  //view the event data in the table
		$answer['UserResult'] = $this->adminModel->getAllUser();   //get all the st_users data (for table)
		
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/index',$answer);
		$this->load->view('common/adminFooter');
	}
///////////////////////////////////////

	function latvalue()
	{
		

     // Get lat and long by address
   // if (!empty($_POST["eventLocation"])) {
   	 $address =  $this->input->post('autocomplete');

           // $address = "NABSID Advertising, Jami Commercial Street 11, Karachi, Pakistan"; // Google HQ
           // echo urlencode($address);
          
        //$prepAddr = str_replace(' ','+',$address);
        $prepAddr = urlencode($address);
       
        
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key='.GOOGLE_MAP_API_KEY);
       
        $output= json_decode($geocode);
        $latitude = $output->results[0]->geometry->location->lat;
        $longitude = $output->results[0]->geometry->location->lng;
        echo $prepAddr;
        echo "<br>";
         echo $latitude.",".$longitude;
         
   // }     
    
         
       



	}
/////////////////////////////////////////////
	function checkUserNameAjax()
{
	
	if(!empty($_POST["userName"])) {
		$userName = $this->input->post('userName');
		$answer = $this->adminModel->checkUserNameAjax($userName);

	if($answer == true) 
	{
		echo "<span class='status-not-available' style='color:red'><i class='fa fa-times' aria-hidden='true'></i> Username Not Available.</span>";
	}
	else 
	{
	echo "<span class='status-available' style='color:green'><i class='fa fa-check' aria-hidden='true'></i> Username Available.</span>";
	}
}   
}
public function checkCurrentPasswordAjax()
{
	
	if(!empty($_POST["password"])) {
		$password = $this->input->post('password');
		$answer = $this->adminModel->checkCurrentPasswordAjax($password);

	if($answer == true) 
	{
		
	}
	else 
	{
		echo "<span class='status-not-available' style='color:red'><i class='fa fa-times' aria-hidden='true'></i> Current password is incorrect.</span>";
	
	}
}
///////////////////////// 
}
	//////////////////////////////////////
	public function testing()
	{
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/index');
		$this->load->view('common/adminFooter');
	}

	public function addCar()                                          //update page bakcend include in this controller
	{
		 $config['upload_path'] = './assets/uploads/images';
         $config['allowed_types']= 'jpg|png';
         $this->load->library('upload', $config);
         $this->upload->do_upload('file_name');
         $file_name = $this->upload->data();

        
	
	
		if(isset($_POST['add_car_submit']) )
		{   
			date_default_timezone_set("Asia/Karachi");
			$now =  date('Y-m-d H:i:s');
			$st_users_data = array(
			'userName' => $this->input->post('userName'),
			'fullName' => $this->input->post('fullName'),
			'fatherName' => $this->input->post('fatherName'),
			'gender' => $this->input->post('gender'),
			'phone' => $this->input->post('phone'),
			'address' => $this->input->post('address'),
			'email' => $this->input->post('email'),
			'cnic' => $this->input->post('cnic'),
			'age' => $this->input->post('age'),
			'license' => $this->input->post('license'),
			'status' => 'active',
			'password' => $this->input->post('password'),
			'userType' => $this->input->post('userType'),  					 //hidden field
			'createDate' => $now,
			'userImage'=>$file_name['file_name'],
			);

			$st_cars_data = array(
			'carModel' => $this->input->post('carModel'),
			'registrationNumber' => $this->input->post('registrationNumber'),
			'color' => $this->input->post('color'),
			'preferenceBranding' => $this->input->post('prefrenceBranding'),
			'manufacturer' => $this->input->post('manufacturer'),
			'trackerId' => $this->input->post('trackerId'),
			'createDate' => $now,
			
			);

			$answer = $this->adminModel->post_addCar($st_users_data,$st_cars_data);    //if only add_car_submit car is set
			
			
		
		}
		else
		{
		$answer['result'] = $this->adminModel->get_existing_car();  				//get user_id and UserName the existing car data
		$answer['result_model'] = $this->adminModel->carModelId();  				//get carModel data
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/addCar',$answer);
		$this->load->view('common/adminFooter');

		}

		
	}
	public function viewCar()           //registered Car 
	{
		$inUse = 1;
		$answer['all_result'] = $this->adminModel->requestApprovals($inUse);  	//get all the assign car data (table)
		$answer['title'] = "Assigned Driver";
		$answer['breadcrumb'] = "Assigned Driver";
		$answer['redirect'] = "viewCar";
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/viewCar',$answer);
		$this->load->view('common/adminFooter');

	}
	public function allCar()           //All Car 
	{
		$inUse = '';
		$answer['all_result'] = $this->adminModel->requestApprovals($inUse);  	//get all the existing car data (table)
		$answer['title'] = "All Driver";
		$answer['breadcrumb'] = "All Driver";
		$answer['redirect'] = "allCar";
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/viewCar',$answer);
		$this->load->view('common/adminFooter');

	}
	public function deleteAllCar()
	{
		
		$answer = $this->adminModel->deleteAllCar($this->input->get('id'),$this->input->get('redirect'));
	}
	public function availableCar()           //available Car 
	{
		$inUse = '0';
		$answer['all_result'] = $this->adminModel->requestApprovals($inUse);  	//get all the existing car data (table)
		$answer['title'] = "Available Driver";
		$answer['breadcrumb'] = "Available Driver";
		$answer['redirect'] = "availableCar";
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/viewCar',$answer);
		$this->load->view('common/adminFooter');

	}

	public function updateViewCar()
	{

		if (isset($_POST['updateCarSubmit'])) 
		{
			
			date_default_timezone_set("Asia/Karachi");
			$now =  date('Y-m-d H:i:s');
			$st_users_data = array(
			'userName' => $this->input->post('userName'),
			'fullName' => $this->input->post('fullName'),
			'fatherName' => $this->input->post('fatherName'),
			'gender' => $this->input->post('gender'),
			'phone' => $this->input->post('phone'),
			'address' => $this->input->post('address'),
			'email' => $this->input->post('email'),
			'cnic' => $this->input->post('cnic'),
			'age' => $this->input->post('age'),
			'license' => $this->input->post('license'),
			
			'password' => $this->input->post('password'),
			// 'userType' => $this->input->post('userType'),  					 //hidden field
			'createDate' => $now,
			// 'userImage'=>$file_name['file_name'],
			);

			$st_cars_data = array(
			// 'carModel' => $this->input->post('carModel'),
			'registrationNumber' => $this->input->post('registrationNumber'),
			'color' => $this->input->post('color'),
			'preferenceBranding' => $this->input->post('prefrenceBranding'),
			'manufacturer' => $this->input->post('manufacturer'),
			'createDate' => $now,
			);
			$carId = $this->input->post('carId'); 
			 $userId = $this->input->post('userId');             			//hidden field in update form
			$answer = $this->adminModel->updateViewCar($userId,$carId,$st_users_data,$st_cars_data);

		}
		else
		{
		$car_id = $this->input->get('id');                              //get car id from url
		$answer['result'] = $this->adminModel->get_viewCar_update($car_id);
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/updateViewCar',$answer);
		$this->load->view('common/adminFooter');
		}
		
	}

	public function individual_viewCar($id)
		{
			$data = $this->adminModel->individual_viewCar($id);
 			echo json_encode($data);
		}

	public function requestApprovals()
	{
		$inUse ='0';
		
		$answer['carRequestId'] = $this->input->get('id');
		 $carRequestId = $this->input->get('id');

		$answer['carResult'] = $this->adminModel->getNoOfCars($carRequestId);   //resultNoOfCars
// 	noOfCars
// noOfAssignedCars
// remainingCars

		$answer['result'] = $this->adminModel->requestApprovals($inUse);

			
			
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/requestApprovals',$answer);
		$this->load->view('common/adminFooter');
	}

	public function updateRequestApprovals()
	{
		$carRequestId = $this->input->get('id');
		$answer['carRequestId']= $carRequestId;
			$answer['result'] = $this->adminModel->updateRequestApprovals($carRequestId);
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/updateRequestApprovals',$answer);
		$this->load->view('common/adminFooter');
	}
	public function assignSingleCar()
	{
			date_default_timezone_set("Asia/Karachi");
			$now =  date('Y-m-d H:i:s');
			$carId = $this->input->get('carId');
			$carRequestId = $this->input->get('request');
		
			
			$createDate = $now;
			
	
	 $answer = $this->adminModel->assignSingleCar($carId,$createDate,$carRequestId);


	}
	public function deleteUpdateApprovals()
	{
		
		$this->adminModel->deleteUpdateApprovals($this->input->get('id'));   //carId
	}
	public function assignSelectedCar()
	{
		date_default_timezone_set("Asia/Karachi");
			$now =  date('Y-m-d H:i:s');
			$selectedCarId = $this->input->post('selected_row');
			$createDate = $now;
			$carRequestId =   $this->input->post('carRequestId');

			
		
	 $answer = $this->adminModel->assignSelectedCar($selectedCarId,$createDate,$carRequestId);
	}

	public function allRequest()
	{
		
		$answer['result'] = $this->adminModel->getAllRequest();
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/allRequest',$answer);
		$this->load->view('common/adminFooter');

	}
	public function allRequestDelete()
	{
		$this->adminModel->allRequestDelete($this->input->get('id'));
	}

	public function carRequest()
	{
		if (isset($_POST['car_request_submit']))
		{
			date_default_timezone_set("Asia/Karachi");
			$now =  date('Y-m-d H:i:s');
			$data = array
			(
			'noOfCars' => $this->input->post('NoOfCar'),
			// 'CarModel' => $this->input->post('CarModel'),
			'preferenceBranding' => $this->input->post('preferenceBranding'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate'),
			'customerId' => $this->input->post('userId'),
			'createDate' => $now,
			);
			$answer = $this->adminModel->carRequest($data);
		}
		else
		{
		$answer['result'] = $this->adminModel->get_users();  //get all Client
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/carRequest',$answer);
		$this->load->view('common/adminFooter');
	}

	}

	public function upadteAllRequestIndividual()
	{
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/upadteAllRequestIndividual');
		$this->load->view('common/adminFooter');

	}
	
	public function existing_addCar()
	{

		if(isset($_POST['existing_car_submit']))
		{   
			date_default_timezone_set("Asia/Karachi");
			$now =  date('Y-m-d H:i:s');
			
			

			$st_cars_data = array(
			'userId' => $this->input->post('userId'),
			// 'carModel' => $this->input->post('carModel'),
			'registrationNumber' => $this->input->post('registrationNumber'),
			'color' => $this->input->post('color'),
			'preferenceBranding' => $this->input->post('prefrenceBranding'),
			'carCompany' => $this->input->post('carCompany'),
			'createDate' => $now,
			
			);
			 $answer = $this->adminModel->post_existing_addCar($st_cars_data);
		
		}
		else
		{
	
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/addCar');
		$this->load->view('common/adminFooter');

		}
	}
	//get all the registered user with its car information
	public function get_view_register_individual()
	{
		echo "string";
	}

	public function addWorkingLocation()
	{
		if (isset($_POST['add_arae_submit'])) 
		{
			$data = array
			(
			'locationText' => $this->input->post('locationText'),
			'longitude' => $this->input->post('longitude'),
			'latitude' => $this->input->post('latitude'),
			);
			 $answer = $this->adminModel->addWorkingLocation($data);
			
		}
		else
		{
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/addWorkingLocation');
		$this->load->view('common/adminFooter');

		}
		
	}
	public function viewWorkingLocation()
	{
		$status = 'active';
		$answer['result'] = $this->adminModel->get_addWorkingLocation($status);   	//getting all the st_working_locations data(for table)
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/viewWorkingLocation',$answer);
		$this->load->view('common/adminFooter');

	}
	public function deleteWorkingLocation()
	{
		if($this->input->get('id') != '')
		{
			$status = array( 
			'status' => "trash",
			);
			$locationId = $this->input->get('id');                 //id = locationId
			$answer['result'] = $this->adminModel->deleteWorkingLocation($status,$locationId);	
		}
		else
		{
			echo "invalid id";
		}
	}

	public function updateWorkingLocation()
	{
		if($this->input->get('id') != '')
		{   
			 	$locationId = $this->input->get('id');
			
	
$answer['result'] = $this->adminModel->get_update_WorkingLocation($locationId);   	//getting all the st_working_locations data(for update)
			$this->load->view('common/adminHeader');
			$this->load->view('common/adminSidebar');
			$this->load->view('admin/updateWorkingLocation',$answer);
			$this->load->view('common/adminFooter');

		}
		else
		{
			if(isset($_POST['updateWorkingLocation_submit']))
		{
			$locationId = $this->input->post('locationId');
			$data = array(
			
			'locationText' => $this->input->post('locationText'),
			'longitude' => $this->input->post('longitude'),
			'latitude' => $this->input->post('latitude'),
			 );
			$answer['result'] = $this->adminModel->updateWorkingLocation($locationId,$data);   	//(for update Working Location)

		}
	}

	}
	public function createUser()
	{
		 // $config['upload_path'] = './assets/uploads/images';
   //       $config['allowed_types']= 'jpg|png';
   //       $this->load->library('upload', $config);
   //       $this->upload->do_upload('file_name');
   //       $file_name = $this->upload->data();

        
	
	
		if(isset($_POST['create_user_submit']))
		{   
			
			date_default_timezone_set("Asia/Karachi");
			$now =  date('Y-m-d H:i:s');
			$data = array
			(
			'userName' => $this->input->post('userName'),
			'fullName' => $this->input->post('fullName'),        //Client name
			// 'fatherName' => $this->input->post('fatherName'),
			// 'gender' => $this->input->post('gender'),
			'phone' => $this->input->post('phone'),
			'address' => $this->input->post('address'),
			'email' => $this->input->post('email'),
			'companyRegistrationNumber' => $this->input->post('companyRegistrationNumber'),
			'since' => $this->input->post('since'),         			  //Since
			// 'passport' => $this->input->post('passport'),
			'status' => 'active',
			'password' => $this->input->post('password'),
			'userType' => $this->input->post('userType'),   		//hidden field
			'createDate' => $now,
			// 'userImage'=>$file_name['file_name'],
			);
			 $answer = $this->adminModel->post_createUser($data);

		
		}
		else
		{
			$this->load->view('common/adminHeader');
			$this->load->view('common/adminSidebar');
			$this->load->view('admin/createUser');
				$this->load->view('common/adminFooter');
		}
		
	}

	public function allUser()
	{
		$answer['result'] = $this->adminModel->getAllUser();   //get all the st_users data (for table)
		$answer['title'] = "All Client";
		$answer['breadcrumb'] = "All Client";

			$this->load->view('common/adminHeader');
			$this->load->view('common/adminSidebar');
			$this->load->view('admin/viewUser',$answer);
			$this->load->view('common/adminFooter');
	}
	public function deleteViewUser()
	{
		$userId = $this->input->get('id');
		$this->adminModel->deleteViewUser($userId);
	}
	public function deleteViewCar()
	{
		$carId = $this->input->get('id');
		$this->adminModel->deleteViewCar($carId);
	}
	public function individualViewUser($id)
	{
		$data = $this->adminModel->individualViewUser($id);
 		echo json_encode($data);
	}


	public function updateViewUser()
	{
		if ($this->input->get('id') != '') {
			
		$userid = $this->input->get('id');                 // get id from url

		$data['result'] = $this->adminModel->individualViewUserData($userid);
	
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/updateViewUser',$data);
		$this->load->view('common/adminFooter');
		}
		else
		{
			redirect('adminController/viewUser');
		}

	}
	//final update of view user data
	public function updateViewUser_submit()
	{
		if (isset($_POST['updateViewUser_submit']))
		 {
		 	$userId = $this->input->post('userId');
			$data = array
			(
			
			'userName' => $this->input->post('userName'),
			'fullName' => $this->input->post('fullName'),        //Client name
		
			'phone' => $this->input->post('phone'),
			'address' => $this->input->post('address'),
			'email' => $this->input->post('email'),
			'companyRegistrationNumber' => $this->input->post('companyRegistrationNumber'),
			'since' => $this->input->post('since'),         			  //Since
		
			);
	
			$answer = $this->adminModel->updateViewUser_submit($userId,$data);
		// $answer = $this->adminModel->updateViewUser_submit();
		}

	}


	public function signUp()
	{
		
		$this->load->view('signUp');
		
	}
	// public function login()
	// {
	// 	if (isset($_POST['login_submit'])) 
	// 	{
			
	// 		$userName = $this->input->post('userName');
	// 		$password = $this->input->post('password');
	// 		$userType = $this->input->post('userType');	   		//hidden field in login form(admin)
	// 		$answer = $this->adminModel->login($userName,$password,$userType);
	// 		if ($answer) 
	// 		{
				
	// 		$session_data = array(
	// 			'userIdAdmin' => $answer[0]['userId'],
	// 			'userNameAdmin'=>$answer[0]['userName'],     		//get userName
	// 			'emailAdmin'=>$answer[0]['email'], 
	// 			'userTypeAdmin'=>$answer[0]['userType'],
	// 			'fullNameAdmin'=>$answer[0]['fullName'],  
				
				
	// 		);
	// 		$this->session->set_userdata($session_data);
	// 		// $this->load->view('login');                         //after login 
	// 		redirect('adminController/login');
	// 		}
	// 		else
	// 		{
	// 			$this->session->set_flashdata('not_login_message', 'Not login');
	// 			redirect('adminController/login');	
	// 		}
		
	// 	}
	// 	else
	// 	{
	// 		$this->load->view('admin/login');
			
	// 	}
		
	// }

	public function change_password()
	{


		if (isset($_POST['cpassword'])) {
         $password = $this->input->post('password');
         $npassword = $this->input->post('npassword');
         $cpassword = $this->input->post('cpassword');
		
		
			$userId = $this->session->userdata('userIdAdmin');    //destroy in logout function
			if ($cpassword != $npassword ) {
				echo "not matched";

			}
			else
			{
				$answer = $this->adminModel->change_password($cpassword,$userId);
				echo "password change successfuly";
			}
			
		}
		
	}
	public function viewEvent()
	{
		$answer['result'] = $this->adminModel->viewEvent();  //view the event data in the table
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/viewEvent',$answer);
		$this->load->view('common/adminFooter');
	}

		public function createEvent()
	{
			if (isset($_POST['create_event_submit'])) 
		{
			date_default_timezone_set("Asia/Karachi");
			$now =  date('Y-m-d H:i:s');
			$data = array
			(
			'eventName' => $this->input->post('eventName'),
			'eventLocation' => $this->input->post('eventLocation'),
			'locLatitude' => $this->input->post('locLatitude'),
			'locLongitude' => $this->input->post('locLongitude'),
			'eventDate' => $this->input->post('eventDate'),
			'eventTime' =>  date("H:i", strtotime($this->input->post('eventTime'))),
			'userId' => $this->input->post('userId'),            //Client id
		        
			 'createDate' => $now,
			 
			 // 'eventStartTime3' =>  date('h:i p',strtotime('22:30:00')),
			);

			   $answer = $this->adminModel->createEvent($data);
		}
		else
		{
		$answer['result'] = $this->adminModel->get_users();  //get all Client
		$this->load->view('common/adminHeader');
		$this->load->view('common/adminSidebar');
		$this->load->view('admin/createEvent',$answer);
		$this->load->view('common/adminFooter');
		}
	}

	public function userCars($userId = false)
	{
		if($userId)
		{
			$answer['result'] = $this->adminModel->userCars($userId);  

			$this->load->view('common/adminHeader');
			$this->load->view('common/adminSidebar');
			$this->load->view('admin/userCars',$answer);
			$this->load->view('common/adminFooter');
		}else{
			echo "invalid request";
		}
	}

public function updateViewEvent()
	{
		if (isset($_POST['updateViewEvent_submit']))
		 {
			date_default_timezone_set("Asia/Karachi");
			$now =  date('Y-m-d H:i:s');
			$eventId = $this->input->post('eventId');
			$data = array
			(
			'eventName' => $this->input->post('eventName'),
			'eventLocation' => $this->input->post('eventLocation'),
			'locLatitude' => $this->input->post('locLatitude'),
			'locLongitude' => $this->input->post('locLongitude'),
		
			'eventTime' => $this->input->post('eventTime'),
			'eventDate' => $this->input->post('eventDate'),
			
			// 'createdBy' => "working",
			// 'status' => "working",
			// 'adminStatus' => "working",
			 'createDate' => $now,
			);

			 $answer = $this->adminModel->updateViewEvent($data,$eventId);

		}
		else
		{
			 $eventId = $this->input->get('id');  
			 $answer['result'] = $this->adminModel->getUpdateViewEvent($eventId);   //get data for update in the input field

			 // echo "<pre>";
			 // echo print_r($answer['result']);
			 // echo "</pre>";

			$this->load->view('common/adminHeader');
			$this->load->view('common/adminSidebar');
			$this->load->view('admin/updateViewEvent',$answer);
			$this->load->view('common/adminFooter');	
		}
		
	}
	public function individualViewEvent($id)
	{
		$answer = $this->adminModel->individualViewEvent($id);
		echo json_encode($answer);
	}

public function viewOnMap()
{
			 $eventId = $this->input->get('event');  
			 $answer['result'] = $this->adminModel->viewOnMap($eventId);   //get lat and long
			 
			$this->load->view('common/adminHeader');
			$this->load->view('common/adminSidebar');
			$this->load->view('admin/viewOnMap',$answer);
			$this->load->view('common/adminFooter');	
}
	// public function approveViewEvent()
	// {
	// 	$eventId = $this->input->get('event');            //get eventId from hidden field
	// 	// $userId = $this->input->post('userId');           // get userId from hidden field 

	// 	$answer = $this->adminModel->approveViewEvent($eventId);
	// }
	public function deleteViewEvent()
	{
		$eventId = $this->input->get('event');            //get eventId from hidden field
		$answer = $this->adminModel->deleteViewEvent($eventId);
	}

	public function defineRate()
	{
			$this->load->view('common/adminHeader');
			$this->load->view('common/adminSidebar');
			$this->load->view('admin/defineRate');
			$this->load->view('common/adminFooter');	
	}
	public function checkAvailabelCars()
	{
		 $noOfCar = $this->input->post('inputNoOfCar');
		 $answer = $this->adminModel->checkAvailabelCars($noOfCar);
		 if ($noOfCar > $answer ) {
		  	echo "<span style='color:red;'> Current we have ".$answer." Available Car</span>";		  	
		  }else{
		  	 echo "0";
		  }
		
	}
	public function logout()
	{
		$this->session->unset_userdata('userIdAdmin');
		$this->session->unset_userdata('userNameAdmin');
		$this->session->unset_userdata('emailAdmin');
		$this->session->unset_userdata('userTypeAdmin');
		$this->session->unset_userdata('fullNameAdmin');

		redirect('adminController/login');

	}
	//change Password 

	public function testing1(){
		    $this->load->view('common/adminHeader');
		    $this->load->view('common/adminSidebar');
			$this->load->view('admin/testing');
			
		// $this->load->view('admin/maptesting');
				 // $this->load->view('common/adminFooter');
	}

	// public function update($id)
	// {
	// 	$answer = $this->adminModel->update($id);
	// }
}

