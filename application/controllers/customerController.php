<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class customerController extends CI_Controller {
	 function __construct() 
	 {
        parent::__construct();
        if (($this->session->userdata('userType') != 'customer') && ($this->session->userdata('userId') == null )) {
        	redirect('loginController/customerLogin');
        }
        echo "string";
        $this->load->model('customerModel');
    }
    public function deleteUpdateApprovals()
	{
		
		$this->customerModel->deleteUpdateApprovals($this->input->get('id'));   //carId
	}

	public function index()
	{
		$userId =  $this->session->userdata('userId');
		$answer['result'] = $this->customerModel->viewEvent($userId);  //view the event data in the table
		$answer['resultCarDetails'] = $this->customerModel->carDetails($userId);
		$answer['totalEvents'] = $this->customerModel->counts($userId);
		$answer['totalCarsRequest'] = $this->customerModel->counts1($userId);
		$answer['totalAssignedCar'] = $this->customerModel->counts2($userId);
		
		$this->load->view('common/customerHeader');
		$this->load->view('common/customerSidebar');
		$this->load->view('customer/index',$answer);
		$this->load->view('common/customerFooter');
	}
	public function testing()
	{
		$this->load->view('common/customerHeader');
		$this->load->view('common/customerSidebar');
		$this->load->view('customer/carRequest');
		$this->load->view('common/customerFooter');
	}
	public function carRequest()
	{
	 if (isset($_POST['car_request_submit']))
		{
			date_default_timezone_set("Asia/Karachi");
			$now =  date('Y-m-d H:i:s');
			$data = array
			(
			'noOfCars' => $this->input->post('NoOfCar'),
			// 'CarModel' => $this->input->post('CarModel'),
			'preferenceBranding' => $this->input->post('preferenceBranding'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate'),
			'customerId' =>$this->session->userdata('userId'),
			'status' =>'pending',
			'createDate' => $now,
			);
			$answer = $this->customerModel->carRequest($data);
		}
		else
		{
			$this->load->view('common/customerHeader');
			$this->load->view('common/customerSidebar');
			$this->load->view('customer/carRequest');
			$this->load->view('common/customerFooter');
		}
		
	}
	public function deleteAllRequest()
	{
		$this->customerModel->deleteAllRequest($this->input->get('id'));   //carId
	}

		public function allRequest()
	{
		$condition = "WHERE sc.customerId =".$this->session->userdata('userId')." AND (sc.status = 'approved' OR sc.status='pending') group by sc.carRequestId";
		
		$answer['result'] = $this->customerModel->getAllRequest($condition);

		$this->load->view('common/customerHeader');
		$this->load->view('common/customerSidebar');
		$this->load->view('customer/allRequest',$answer);
		$this->load->view('common/customerFooter');
	}

		public function updateAllRequest()
	{
		if (isset($_POST['updateAllRequest_submit'])) 
		{
			$carRequestId = $this->input->post('carRequestId');
			$data = array(
			'noOfCars' => $this->input->post('NoOfCar'),
			
			'preferenceBranding' => $this->input->post('preferenceBranding'),
			'startDate' => $this->input->post('startDate'),
			'endDate' => $this->input->post('endDate'),
			'customerId' =>$this->session->userdata('userId'),
			);
			$this->customerModel->updateAllRequest($data,$carRequestId);
		}
		else
		{
			$condition = "WHERE carRequestId =". $this->input->get('id');  //carRequestId
		
		
		$answer['result'] = $this->customerModel->getAllRequest($condition);
		$this->load->view('common/customerHeader');
		$this->load->view('common/customerSidebar');
		$this->load->view('customer/updateAllRequest',$answer);
		$this->load->view('common/customerFooter');
		}
		
		
		
	}
	
		public function carDetails()
	{

		$userId = $this->session->userdata('userId');
		$answer['result'] = $this->customerModel->carDetails($userId);
		$this->load->view('common/customerHeader');
		$this->load->view('common/customerSidebar');
		$this->load->view('customer/carDetails',$answer);
		$this->load->view('common/customerFooter');
	}
	public function onMap()
	{

		$this->load->library('googlemap');
		$latitudeA = 24.9411388;
		$longitutdeA = 67.0397909;

		$latitudeB = 24.8296058;
		$longitutdeB = 67.0684178;

		
		$config['zoom'] = '1';
				// $config['trafficOverlay'] = TRUE;
		$config['directions'] = TRUE;
		$config['directionsStart'] = $latitudeA.','.$longitutdeA;
		$config['directionsEnd'] = $latitudeB.','.$longitutdeB;
		$config['directionsDivID'] = 'directionsDiv';
		$this->googlemap->initialize($config);
		$data['map'] = $this->googlemap->create_map();

		$this->load->view('common/customerHeader');
		$this->load->view('common/customerSidebar');
		$this->load->view('customer/onMap',$data);
		$this->load->view('common/customerFooter');
	}
	public function createEvent()
	{
		if (isset($_POST['create_event_submit'])) 
		{
			date_default_timezone_set("Asia/Karachi");
			$now =  date('Y-m-d H:i:s');
			$data = array
			(
			'eventName' => $this->input->post('eventName'),
			'eventLocation' => $this->input->post('eventLocation'),
			'locLatitude' => $this->input->post('locLatitude'),
			'locLongitude' => $this->input->post('locLongitude'), 
			'eventDate' => date("Y-m-d", strtotime($this->input->post('eventDate'))),
			'eventTime' =>  date("H:i", strtotime($this->input->post('eventTime'))),
			'userId' => $this->session->userdata('userId'),              //userId session destroy in logout        
			'createDate' => $now,
			 
			 // 'eventStartTime3' =>  date('h:i p',strtotime('22:30:00')),
			);
			   $answer = $this->customerModel->createEvent($data);
		}
		else
		{	
			$answer['result'] = $this->customerModel->noOfAssignedCars();   //get only number of assigned cars
			 
		$this->load->view('common/customerHeader');
		$this->load->view('common/customerSidebar');
		$this->load->view('customer/createEvent',$answer);
		$this->load->view('common/customerFooter');
		}
	}

	public function viewOnMap()
{
			 $eventId = $this->input->get('event');  
			 $answer['result'] = $this->customerModel->viewOnMap($eventId);   //get lat and long
			 
			$this->load->view('common/adminHeader');
			$this->load->view('common/adminSidebar');
			$this->load->view('admin/viewOnMap',$answer);
			$this->load->view('common/adminFooter');	
}
	
	public function viewEvent()
	{
		$userId =  $this->session->userdata('userId');
		$answer['result'] = $this->customerModel->viewEvent($userId);  //view the event data in the table
		$this->load->view('common/customerHeader');
		$this->load->view('common/customerSidebar');
		$this->load->view('customer/viewEvent',$answer);
		$this->load->view('common/customerFooter');
	}

	public function deleteViewEvent()
	{
		$eventId = $this->input->get('event');            //get eventId from hidden field
		$answer = $this->customerModel->deleteViewEvent($eventId);

	}
	public function individualViewEvent($id)
	{
		$answer = $this->customerModel->individualViewEvent($id);
		echo json_encode($answer);
	}

	public function updateViewEvent()
	{
		if (isset($_POST['updateViewEvent_submit']))
		 {
			date_default_timezone_set("Asia/Karachi");
			$now =  date('Y-m-d H:i:s');
			$eventId = $this->input->post('eventId');
			$data = array
			(
			'eventName' => $this->input->post('eventName'),
			'eventLocation' => $this->input->post('eventLocation'),
			'locLatitude' => $this->input->post('locLatitude'),
			'locLongitude' => $this->input->post('locLongitude'),
			'eventDate' => $this->input->post('eventDate'),
			'eventTime' => $this->input->post('eventTime'),
			'userId' => $this->session->userdata('userId'),                      //createdId
			// 'status' => "Pending",
			'adminStatus' => "Pending Request",
			 'createDate' => $now,
			);

			 $answer = $this->customerModel->updateViewEvent($data,$eventId);

		}
		else
		{
			 $eventId = $this->input->get('id');  
			 $answer['result'] = $this->customerModel->getUpdateViewEvent($eventId);   //get data for update in the input field

			$this->load->view('common/customerHeader');
			$this->load->view('common/customerSidebar');
			$this->load->view('customer/updateViewEvent',$answer);
			$this->load->view('common/customerFooter');	
		}
		
	}

	public function impression()
	{
		$this->load->view('common/customerHeader');
		$this->load->view('common/customerSidebar');
		$this->load->view('customer/impression');
		$this->load->view('common/customerFooter');
	}
		public function editProfile()
	{
		$this->load->view('common/customerHeader');
		$this->load->view('common/customerSidebar');
		$this->load->view('customer/editProfile');
		$this->load->view('common/customerFooter');
	}


	
	public function assignedCarDetails()
	{
		$answer['result'] = $this->customerModel->assignCarDetails($this->input->get('id'));
		$this->load->view('common/customerHeader');
		$this->load->view('common/customerSidebar');
		$this->load->view('customer/assignedCarDetails',$answer);
		$this->load->view('common/customerFooter');
		// echo "string";

	}

	public function change_password()
	{
		if (isset($_POST['change_password_submit'])) {
			$cpassword = $this->input->post('cpassword');
			$npassword = $this->input->post('npassword');
			$userId = $this->session->userdata('userId');    //destroy in logout function
			if ($cpassword != $npassword ) {
				$this->session->set_flashdata('not_match_pass_message', 'Password do not match');
				redirect('customerController/carRequest');	

			}
			else
			{
				 $this->customerModel->change_password($cpassword,$userId);

			}
			
		}
		
	}
		public function signOut()
	{
		{
		$this->session->unset_userdata('userId');
		$this->session->unset_userdata('userName');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('userType');
		$this->session->unset_userdata('fullName');

		redirect('customerController/customerLogin');

		}

	
	}

}
	?>
