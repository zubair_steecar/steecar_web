<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class loginController extends CI_Controller {
	 function __construct() 
	 {
        parent::__construct();
        $this->load->model('adminModel');
        $this->load->model('customerModel');
    }

	public function index()
	{
		$this->load->view('index');
	}

//Admin Login
	public function login()
	{
		if (isset($_POST['login_submit'])) 
		{
			
			$userName = $this->input->post('userName');
			$password = $this->input->post('password');
			$userType = $this->input->post('userType');	   		//hidden field in login form(admin)
			$answer = $this->adminModel->login($userName,$password,$userType);
			if ($answer) 
			{
				
			$session_data = array(
				'userIdAdmin' => $answer[0]['userId'],
				'userNameAdmin'=>$answer[0]['userName'],     		//get userName
				'emailAdmin'=>$answer[0]['email'], 
				'userTypeAdmin'=>$answer[0]['userType'],
				'fullNameAdmin'=>$answer[0]['fullName'],  
				
				
			);
			$this->session->set_userdata($session_data);
			// $this->load->view('login');                         //after login 
			redirect('loginController/login');
			}
			else
			{
				$this->session->set_flashdata('not_login_message', 'Not login');
				redirect('loginController/login');	
			}
		
		}
		else
		{
			$this->load->view('admin/login');
			
		}
		
	}

//Customer LOgin 
	public function customerLogin()
	{
		if (isset($_POST['customer_login_submit'])) 
		{
			
			$userName = $this->input->post('userName');
			$password = $this->input->post('password');
			$userType = $this->input->post('userType');	   		//hidden field in login form(customer)
			$answer = $this->customerModel->customerLogin($userName,$password,$userType);
			if ($answer) 
			{
				
			$session_data = array(
				'userId' => $answer[0]['userId'],
				'userName'=>$answer[0]['userName'],     		//get userName
				'email'=>$answer[0]['email'], 
				'userType'=>$answer[0]['userType'],
				'fullName'=>$answer[0]['fullName'],  
				
				
			);
			$this->session->set_userdata($session_data);
			// $this->load->view('login');                         //after login 
			redirect('loginController/customerLogin');
			}
			else
			{
				$this->session->set_flashdata('not_login_message', 'Please!Enter Correct username and Password');
				redirect('loginController/customerLogin');	
			}
		
		}
		else
		{
			$this->load->view('customer/customerLogin');
			
		}
		
	}
}

?>