<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('index');
	}
	public function testing()
	{
		$this->load->view('common/header');
		$this->load->view('common/sidebar');
		$this->load->view('index');
		$this->load->view('common/footer');
	}

	public function another_test()
	{
		$this->load->view('common/header');
		$this->load->view('common/sidebar');
		$this->load->view('index');
		$this->load->view('common/footer');
	}
}

